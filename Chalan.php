<?php
	$conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
	$fno= $_GET['id'];
	$sql = "SELECT * FROM `admission` WHERE `ID` =  ".$fno;
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
    while($row = mysqli_fetch_assoc($result)) {

		$id									=	$row["ID"];
		$STUDENT_NAME						=	$row["STUDENT_NAME"];
		$MOBILE								=	$row["MOBILE"];
		$DATE								=	substr($row["DATESTAMP"],0,10);
		$GEN_HONOURS						=	$row["GEN_HONOURS"];		


    }  } else {   echo " Some Error ! "; exit();}
	//$SID='H16'.$_GET['f'];	//Hons-Session 16-- 2char Hons Subject --RESERVATION_CATEGORY--SL
	//if($GENORHONS=="stream1")$HNSS="BENGALI";if($GENORHONS=="stream2")$HNSS="ENGLISH";if($GENORHONS=="stream3")$HNSS="EDUCATION";if($GENORHONS=="stream4")$HNSS="POL. SC.";if($GENORHONS=="stream5")$HNSS="SOCIOLOGY";if($GENORHONS=="stream6")$HNSS="GENERAL";

  ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<!--<link rel="stylesheet" href="/asset/css/admission_form.css" type="text/css">-->
		<style>			
			.header-block{
				padding: 5px;
				display: table;
				border: 1px solid #eee;
			}
			.text-block h1{
				color: #032ba2;
				margin-bottom: 3px;
				font-size: 20px;
				font-family: 'Spectral SC', serif;
				text-decoration: underline;
			}			
			.table{    
				border-collapse: collapse;
				width: 100%;
				text-transform: capitalize;
			}
			.table td,
			.table th {
				border: 1px solid #ddd;
				padding: 4px;
			}
			.table tr:nth-child(even) {
				background-color: #f2f2f2;
			}

			.table tr:hover {
				background-color: #ddd;
			}

			.table th {
				padding-top: 8px;
				padding-bottom: 8px;
				text-align: left;
				background-color: #ff9742;
				color: white;
			}
			.form_header{
				height:250px;
			}
			.form_header_txt{
				width:500px;
			}
			.logo_txt{
				font-size:36px;
				text-align:center;
				color:#0973B3;
			}
			.small_txt_center{
				width:500px;
				text-align:center;
				font-size:.9em;
			}
			.sbmt{
				margin-top:15px;
				padding:10px;
			}
			form#ug_admission > fieldset > input , select.inpt_01 {
				width:740px;
				margin-bottom:15px;
				border-top:0;
				border-left:0px;
				border-bottom:2px solid #B4B0F9;
				font-size:1.3em;
				text-transform:uppercase;
			}
		</style>
	</head>
	<body>
		<table class="table" border="1"  style="border-collapse: separate">
			<tr>
				<td style="width:50px;border:none;text-align:center;">					
					<img style="width:60px;" src="/asset/img/amdanga_logo.png" alt="logo">				
				</td>
				<td class="text-block" colspan="2" style="border:none;">					
						<h1>DEMO COLLEGE NAME</h1>
				</td>			
			</tr>
			<tr>
				<td><center><b>Admission Form Chalan</b> - Bank Copy -<b> ( <?php echo $GEN_HONOURS;?> )</center></td>
				<td><center><b>Admission Form Chalan</b> - Student Copy -<b> ( <?php echo $GEN_HONOURS;?> ) </b></center></td>
				<td><center><b>Admission Form Chalan</b> - College Copy -<b> ( <?php echo $GEN_HONOURS;?> )</center></td>
			</tr>
			<tr>
				<td width="250px">
					<table border="1" style="border-collapse: separate;width:100%;">						
						<tr>
							<td>Bank</td>
							<td>UBI Bank</td>
						</tr>
						<tr>
							<td>Account to be credited</td>
							<td>1595010061755</td>
						</tr>
						<tr>
							<td>Institution Name</td>
							<td>Sree Chaitanya College</td>
						</tr>						
						<tr>
							<td colspan="2"><center>Form No : <b>SCC19UG<?php echo $id;?></b></center></td>
						</tr>
						<tr>
							<td>Student's Name</td>
							<td><?php echo $STUDENT_NAME;?></td>
						</tr>
						<tr>
							<td>Mobile</td>
							<td><?php echo $MOBILE;?></td>
						</tr>
						<tr>
							<td>Amount</td>
							<td>300</td>
						</tr>
						<tr>
							<td>Transaction Charge</td>
							<td>46</td>
						</tr>
						<tr>
							<td>Total to be Paid</td>
							<td>346</td>
						</tr>						
						<tr>
							<td>Transaction ID</td>
							<td></td>
						</tr>
						<tr>
							<td>Transaction Date</td>
							<td></td>
						</tr>
						<tr>
							<td>Name of Branch</td>
							<td></td>
						</tr>
						<tr>
							<td>Registration Date</td>
							<td><?php echo $DATE;?></td>
						</tr>
						<tr>
							<td><br><br>Student's Sign</td>
							<td><br><br>Bank Stamp & Sign</td>
						</tr>
					</table>
				</td>
				<td width="250">
					<table border="1" style="border-collapse: separate;width:100%;">						
						<tr>
							<td>Bank</td>
							<td>UBI Bank</td>
						</tr>
						<tr>
							<td>Account to be credited</td>
							<td>1595010061755</td>
						</tr>
						<tr>
							<td>Institution Name</td>
							<td>Sree Chaitanya College</td>
						</tr>						
						<tr>
							<td colspan="2"><center>Form No : <b>SCC19UG<?php echo $id;?></b></center></td>
						</tr>
						<tr>
							<td>Student's Name</td>
							<td><?php echo $STUDENT_NAME;?></td>
						</tr>
						<tr>
							<td>Mobile</td>
							<td><?php echo $MOBILE;?></td>
						</tr>
						<tr>
							<td>Amount</td>
							<td>300</td>
						</tr>
						<tr>
							<td>Transaction Charge</td>
							<td>46</td>
						</tr>
						<tr>
							<td>Total to be Paid</td>
							<td>346</td>
						</tr>						
						<tr>
							<td>Transaction ID</td>
							<td></td>
						</tr>
						<tr>
							<td>Transaction Date</td>
							<td></td>
						</tr>
						<tr>
							<td>Name of Branch</td>
							<td></td>
						</tr>
						<tr>
							<td>Registration Date</td>
							<td><?php echo $DATE;?></td>
						</tr>
						<tr>
							<td><br><br>Student's Sign</td>
							<td><br><br>Bank Stamp & Sign</td>
						</tr>
					</table>
				</td>
				<td width="250">
					<table border="1" style="border-collapse: separate;width:100%;">						
						<tr>
							<td>Bank</td><td>UBI Bank</td>
						</tr>
						<tr>
							<td>Account to be credited</td>
							<td>1595010061755</td>
						</tr>
						<tr>
							<td>Institution Name</td>
							<td>Sree Chaitanya College</td>
						</tr>						
						<tr>
							<td colspan="2"><center>Form No : <b>SCC19UG<?php echo $id;?></b></center></td>
						</tr>
						<tr>
							<td>Student's Name</td>
							<td><?php echo $STUDENT_NAME;?></td>
						</tr>
						<tr>
							<td>Mobile</td>
							<td><?php echo $MOBILE;?></td>
						</tr>
						<tr>
							<td>Amount</td>
							<td>300</td>
						</tr>
						<tr>
							<td>Transaction Charge</td>
							<td>46</td>
						</tr>
						<tr>
							<td>Total to be Paid</td>
							<td>346</td>
						</tr>						
						<tr>
							<td>Transaction ID</td>
							<td></td>
						</tr>
						<tr>
							<td>Transaction Date</td>
							<td></td>
						</tr>
						<tr>
							<td>Name of Branch</td>
							<td></td>
						</tr>
						<tr>
							<td>Registration Date</td>
							<td><?php echo $DATE;?></td>
						</tr>
						<tr>
							<td><br><br>Student's Sign</td>
							<td><br><br>Bank Stamp & Sign</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3"><center>** Form no. will be used for Banking Record. * Chalan valid upto 06.08.16, Print this Chalan in <b>Landscape mode</b>, *Students May Contact on 9932326940 </center></td>
			</tr>
		</table>
	</body>
</html>
