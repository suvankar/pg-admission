<?php
if (isset($_SESSION['email'])) {
    $conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $s = $_SESSION['email'];
    $sql = "SELECT * FROM `scc_pg_19` WHERE `LOGIN_EMAIL` LIKE '%" . $s . "%'" . "";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        echo '<div class="container table-responsive text-center">
				<h1>' . $s . '</h1><hr>
				<table class="table table-bordered table-striped table-hover">
					<tr>
						<th>SL</th>
						<th>FORM ID (print form)</th>
						<th>STUDENT NAME</th>
						<th>MOBILE NO.</th>
						<th>APPLIED FOR</th>
						<th>Form Fee/Status</th>
						<th>Admission Fee/Status</th>
					</tr>';
        $i = 0;
        $t01 = 0;
        $t02[0] = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $i++;
            $id = $row["ID"];
            $STUDENT_NAME = $row["NAME"];
            $MOBILE = $row["MOBILE"];
            $H_NM = $row["H_NM"];
            $AGGREGATE = $row["AGGREGATE"];
            $PAY_STATUS = $row["PAY_FORM"];
            if($row["H_NM"]=='Bengali') $admission_amount=5710;else $admission_amount=9900;
            if($row["PAY_ADMISSION"]==NULL) $PAY_ADMISSION='Not Paid <!--, <a class="btn btn-primary" target="blank" href="/PG_Admission_19/payment_pg_Admission?id='.$id.'&amount='.$admission_amount.'">MAKE PAYMENT</a> -->'; else $PAY_ADMISSION="Paid";

            //$MERIT_LIST = $row["MERIT_LIST"];
            echo '
			<tr>
				<td>' . $i . '</td>
				<td><a href="/PG_Admission_19/PRINT/print_form_pg?id='.$id.'" class="btn btn-primary" target="blank">SCC19PG'.$id.'</a></td>
				<td>' . $STUDENT_NAME . '</td>
				<td>' . $MOBILE . '</td>
				<td>' . $H_NM . '</td>
            ';
            if($row["PAY_FORM"] === 'PAID') {
                echo '<td><p class="btn btn-success">PAID</p></td>';
            } else {
                echo '<td>Not Paid, <a class="btn btn-primary" target="blank" href="/PG_Admission_19/payment_pg?id=' . $id . '">MAKE PAYMENT</a></td>';
            }

            if($row["PAY_ADMISSION"] === 'PAID') {
              echo '<td> Paid </td>';
            }
            // else echo "<td> Not Paid </td>";
            elseif($row["MERIT_LIST"] === NULL) {
                echo '<td> Not Listed </td>';
            } else {
                echo '<td>'.$admission_amount.' '.$PAY_ADMISSION.'</td>';
            }
        }
        echo '	</tr></table></div>';
    } else {
        echo "<div class='container text-center'><h2>No form found within your Gmail!</h2></div>";
    }
}
?>
