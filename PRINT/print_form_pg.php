<?php
	$conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
	$fno= $_GET['id'];
	$sql = "SELECT * FROM `scc_pg_19` WHERE `id` =  ".$fno;
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		while($row = mysqli_fetch_assoc($result)) {

			$STUDENT_NAME						=	$row["NAME"];
			$MOBILE								=	$row["MOBILE"];
			$EMAIL								=	$row["EMAIL"];
			$FATHER_NAME						=	$row["FATHER_NAME"];
			$GUARDIAN_NAME						=	$row["GUARDIAN_NAME"];
			$ADDRESS							=	nl2br($row["ADDRESS"]);
			$DOB								=	$row["DOB_DATE"] .'/'. $row["DOB_MONTH"] . '/' . $row["DOB_YEAR"];
			$GENDER								=	$row["GENDER"];
			$RELIGION							=	$row["RELIGION"];
			$CASTE				                =	$row["CASTE"];
			$DISABILITY					        =	$row["DISABILITY"];
			$BLOOD_GROUP						=	$row["BLOOD_GROUP"];
			$UNIVERSITY							=	$row["UNIVERSITY"];
			$COLLEGE							=	$row["COLLEGE"];
			$H_NM						        =	$row["H_NM"];
			$PASS_YEAR						    =	$row["YEAR_OF_PASSING"];
			$HS_SUB_NAME1						=	$row["H_NM"];
			$HS_SUB_MARK1						=	$row["H_MO"];
			$HS_SUB_NAME2						=	$row["G1_NM"];
			$HS_SUB_MARK2						=	$row["G1_MO"];
			$HS_SUB_NAME3						=	$row["G2_NM"];
			$HS_SUB_MARK3						=	$row["G2_MO"];
			//$Aggregate                          =   ($HS_SUB_MARK1 + $HS_SUB_MARK2 + $HS_SUB_MARK3);
		
		}  
	}
	else {echo " Some Error ! "; exit();}		
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="/asset/css/admission_form.css" type="text/css">
</head>
<body>
	<div class="admission_form">
		<div class="header-block">
			<div class="img-block">
				<img class="logo_img" src="/asset/img/scc_logo.png" alt="logo">
			</div>
			<div class="text-block">
				<h1>Sreechaitanya College</h1>
				<p>Habra(Arts & Science)</p>
				<p>Prafullanagar, Habra, 24PGS(N) PIN-743268, WB</p>
			</div>
		</div>		
		<hr class="hr_margin"><center style="background-color:#ff9742;color:white;padding:5px;">PG ADMISSION FORM 2019-20 </center><hr class="hr_margin">
		<table><td style="width:75%"> Session : 2019-20 </td><td style="width:25%"> Form No: SCC19PG<?php echo $fno;?><td></table>

		<table width="100%">
			<tr>
				<td width="130px">
					<img src="/CONTENT/ROOT_URI/PG_Admission_19/uploads/photo/<?php echo $fno;?>.jpg" style="width:100%;height:auto;border-radius:3px;border:1px solid #f3f3f3;">
				</td>
				<td>
					<table class="table" height="150px">
						<tr>
							<td  width=50%>Student's Name</td>
							<td><?php echo $STUDENT_NAME;?></td>							
						</tr>							
						<tr>
							<td>Applying For</td>
							<td><?php echo $H_NM. ' (YOP - '.$PASS_YEAR.')';?></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>		
		<table class="table">
			<tr>
				<td width=50%>Gender : <?php echo $GENDER;?></td>
				<td>Father's Name : <?php echo $FATHER_NAME;?></td>
			</tr>
			<tr>
				<td>Date Of Birth : <?php echo $DOB;?></td>
				<td>Mobile No : <?php echo $MOBILE;?></td>
			</tr>			
			<tr>
				<td>E-mail : <?php echo $EMAIL;?></td>
				<td>Blood Group : <?php echo $BLOOD_GROUP;?></td>
			</tr><tr>
				<td>Reservation Category : <?php echo $CASTE;?></td>
				<td>Physical Disability : <?php echo $DISABILITY;?></td>
			</tr>
			<tr>
				<td>NAME of The University : <?php echo $UNIVERSITY;?></td>
				<td>NAME of The College : <?php echo $COLLEGE;?></td>
			</tr>
			<tr>				
				<td colspan="2" style="text-align:left;">Address : <?php echo $ADDRESS;?></td>
			</tr>
		</table><br>
		<table class="table" style="width:500px; margin-left:auto;margin-right:auto;">
			<tr>
				<th>Applied Subjects</th>
				<th>Marks Obtained</th>				
			</tr>
			<tr>				
				<td  width=50%><?php echo $HS_SUB_NAME1;?></td>
				<td><?php echo $HS_SUB_MARK1;?></td>
			</tr>
			<tr>				
				<td><?php echo $HS_SUB_NAME2;?></td>
				<td><?php echo $HS_SUB_MARK2;?></td>
			</tr>
			<tr>				
				<td><?php echo $HS_SUB_NAME3;?></td>
				<td><?php echo $HS_SUB_MARK3;?></td>
			</tr>
		</table><br>
		<table>
			<tr>
				<td width="500px"></td>
				<td><img src="/CONTENT/ROOT_URI/PG_Admission_19/uploads/sign/<?php echo $fno;?>.jpg" style="width:auto;height:75px;"></td>
			</tr>
		 	<tr>
			 	<td>Print Date : <?php echo date("Y-m-d");?></td><td><center>Student's Signature<center></td>
			</tr>
		</table>
	</div>
<body>
</html>
