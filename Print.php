<?php
include_once '_config.php';
	$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
	$fno= $_GET['id'];
	$sql = "SELECT * FROM `".PG21S."` WHERE `id` =  ".$fno;
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) ==1) {
		while($row = mysqli_fetch_assoc($result)) {

			$NAME								=	$row["NAME"];
			$MOBILE								=	$row["MOBILE"];
			$EMAIL								=	$row["EMAIL"];
			$FATHER_NAME						=	$row["FATHER_NAME"];
			$GUARDIAN_NAME						=	$row["GUARDIAN_NAME"];
			$ADDRESS							=	nl2br($row["ADDRESS"]);
			$DOB_DATE					        =	$row["DOB_DATE"];
			$DOB_MONTH					        =	$row["DOB_MONTH"];
			$DOB_YEAR					        =	$row["DOB_YEAR"];
			$GENDER					            =	$row["GENDER"];
			$RELIGION					        =	$row["RELIGION"];
			$CASTE					            =	$row["CASTE"];
			$DISABILITY					        =	$row["DISABILITY"];
			$BLOOD_GROUP					    =	$row["BLOOD_GROUP"];
			$BLOOD_DONATED					    =	$row["BLOOD_DONATED"];
			$UNIVERSITY					        =	$row["UNIVERSITY"];
			$COLLEGE					        =	$row["COLLEGE"];
			$YEAR_OF_PASSING					=	$row["YEAR_OF_PASSING"];
			$H_NM					            =	$row["H_NM"];
			$H_FM					            =	$row["H_FM"];
			$H_PM					            =	$row["H_PM"];
			$H_MO					            =	$row["H_MO"];
			$G1_NM					            =	$row["G1_NM"];
			$G1_FM					            =	$row["G1_FM"];
			$G1_PM					            =	$row["G1_PM"];
			$G1_MO					            =	$row["G1_MO"];
			$G2_NM					            =	$row["G2_NM"];
			$G2_FM					            =	$row["G2_FM"];
			$G2_PM					            =	$row["G2_PM"];
			$G2_MO					            =	$row["G2_MO"];
			
			$created_at							=	substr($row["created_at"],0,10);
			$AGGREGATE							=	$row["AGGREGATE"];


		}  
	}
	else {echo mysqli_error($conn)," Some Error ! "; exit();}		
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="/asset/css/admission_form.css" type="text/css">
</head>
<body  onload="window.print()">
	<div class="admission_form">
		<!-- <div class="header-block">
			<div class="img-block">
				<img class="logo_img" src="/asset/img/amdanga_logo.png" alt="logo">
			</div>
			<div class="text-block">
				<h1>DEMO COLLEGE NAME</h1>
				<p>TAG 1</p>
				<p>TAG 2</p>
			</div>
		</div>		 -->
		<hr class="hr_margin"><center style="background-color:#ff9742;color:white;padding:5px;">Sree Chaitanya College, Habra- PG admission form 2021-23 for <?php echo $H_NM;?> </center><hr class="hr_margin">
		<table><td style="width:75%"> Session : 2021-23 </td><td style="width:25%"> Form No: SCC21PG- <?php echo $fno;?> <td></table>

		<table width="100%">
			<tr>
				<td width="130px">
					<img src="/uploads/photo/<?php echo $fno;?>.jpg" style="width:100%;height:auto;border-radius:3px;border:1px solid #f3f3f3;">
				</td>
				<td>
					<table class="table" height="150px">
						<tr>
							<td  width=50%>Student ID - </td>
							<td><?php echo 'SCC21PG'.$fno;?></td>							
						</tr>							
						<tr>
							<td>Applying For - </td>
							<td><?php echo $H_NM;?></td>
						</tr>
						<!-- <tr>
							<td>Aggregate Marks - </td>
							<td><?php echo $AGGREGATE;?> </td>
						</tr> -->
					</table>
				</td>
			</tr>
		</table>		
		<table class="table">
			<tr>
				<td width=50%>Student's Name : <?php echo $NAME;?></td>
				<td>Father's Name : <?php echo $FATHER_NAME;?></td>
			</tr>
			<tr>
				<td>Date Of Birth : <?php echo $DOB_DATE,'.', $DOB_MONTH,'.', $DOB_YEAR ;?></td>
				<td>Gender : <?php echo $GENDER;?></td>
			</tr>
			<tr>
				<td>Reservation Category : <?php echo $CASTE;?></td>
				<td>Physical Disability : <?php echo $DISABILITY;?></td>
			</tr>
			<tr>
				<td> Email : <?php echo $EMAIL;?></td>
				<td>Mobile No : <?php echo $MOBILE;?></td>
			</tr>
			<tr>
				<td>  COLLEGE : <?php echo $COLLEGE;?> </td>
				<td> UNIVERSITY : <?php echo $UNIVERSITY;?></td>
			</tr>
			<tr>
				<td>Year of Passing : <?php echo $YEAR_OF_PASSING;?></td>
				<td>Address : <?php echo $ADDRESS;?></td>
			</tr>
		</table><br>
		<table class="table" style="width:500px; margin-left:auto;margin-right:auto;">
			<tr>
				
				<tr>Marks Obtained: <b> <?php echo $H_NM,': ',$H_MO, ', ', $G1_NM, ': ',$G1_MO, ', ',  $G2_NM, ': ', $G2_MO, ' .';?> </b> </tr>	<hr>			
			</tr>
		</table><br>
		<table>
			<tr>
				<td width="500px">Date Of Submission : <?php echo $created_at;?></td>
				<td><img src="/uploads/sign/<?php echo $fno;?>.jpg" style="width:auto;height:75px;"></td>
			</tr>
		 	<tr>
			 	<td>Print Date : <?php echo date("Y-m-d");?></td><td><center>Student's Signature<center></td>
			</tr>
		</table>
	</div>
<body>
</html>
