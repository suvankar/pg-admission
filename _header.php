<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="keywords" content="Online PG Admission 2022-24">
<meta name="description" content="Online PG Admission 2022-24 for Chemistry Bengali Anthropology">
<meta name="author" content="ov" >
<link rel="stylesheet" href="/assets/bootstrap.min.css">
<script src="/assets/jquery.min.js"></script>
<script src="/assets/bootstrap.min.js"></script>

<title>Online Admission 2021-23</title>
<style>
.wrapper {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    background: #1d1d1d;
    background-image: url(/asset/img/stripes-tile.png);
}

.banner-wrapper {
    max-width: 1200px;
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
}

.div-block {
    display: table-cell;
    vertical-align: middle;
    width: 50%;
    text-align: center;
    padding: 0 25px;
}

.div-block span {
    margin: 25px 0px !important;
}

.header-block {
    padding: 10px;
    display: table;
    width:100%;
}

.img-block {
    display: table-cell;
    vertical-align: middle;
    width: 30%;
    text-align: center;
}

.text-block {
    display: table-cell;
    vertical-align: middle;
    /*width: 60%;*/
}

.text-block h2 {
    color: #e97324;
    margin-bottom: 3px;
    font-family: 'Spectral SC', serif;
    text-decoration: underline;
}

.text-block h5 {
    color: #fff;
    margin-top: 5px;
    margin-bottom: 5px;
}

.text-block h6 {
    color: #fff;
    margin-top: 0px;
    margin-bottom: 5px;
}

.logo_img {
    width: 120px;
    border-radius: 50%;
}
.button {
border-radius: 4px;
background-color: #4CAF50;
border: none;
color: #FFFFFF;
text-align: center;
font-size: 18px;
padding: 10px 15px;
width: 200px;
transition: all 0.5s;
cursor: pointer;
margin: 5px;
}
.button span {
cursor: pointer;
display: inline-block;
position: relative;
transition: 0.5s;
margin-top:25px;
}
.button span:after {
content: '\00bb';
position: absolute;
opacity: 0;
top: 0;
right: -20px;
transition: 0.5s;
}
.button:hover span {
padding-right: 25px;
}

.button:hover span:after {
opacity: 1;
right: 0;
}
</style>
<body>
	<!-- <div class="wrapper">
		<div class="banner-wrapper">
			<div class="header-block">
				<div class="img-block">
					<a href="/"><img class="logo_img" src="/CONTENT/ROOT_URI/Admission/variables/amdanga_logo.png" alt="logo"></a>
				</div>
				<div class="text-block">
					<h2><?php include ("variables/college_name.conf");?></h2>
					<h5><?php include ("variables/college_tag.conf");?></h5>			
					<h6><?php include ("variables/college_address.conf");?></h6>			
				</div>
			</div>
		</div>
	</div> -->
	<?php /* $ua=getBrowser(); 
		if($ua['Device']=="Mobile") include "menu.php";
		else include "menu.php";
	*/ ?>	

