<?php

	if( isset($_POST['formid']) && $_POST['formid']=='PG_21')
	{         // var_dump($_POST);
        $admission["NAME"]					                =	$_POST["NAME"];
        $admission["MOBILE"]					            =	$_POST["MOBILE"];
        $admission["EMAIL"]					                =	$_POST["EMAIL"];
        $admission["LOGIN_EMAIL"]					        =	$_SESSION['email'];
        $admission["FATHER_NAME"]					        =	$_POST["FATHER_NAME"];
        $admission["GUARDIAN_NAME"]					        =	$_POST["GUARDIAN_NAME"];          
        $admission["ADDRESS"]					            =	$_POST["ADDRESS"];
        $admission["DOB_DATE"]					            =	$_POST["DOB_DATE"];
        $admission["DOB_MONTH"]					            =	$_POST["DOB_MONTH"];
        $admission["DOB_YEAR"]					            =	$_POST["DOB_YEAR"];
        $admission["GENDER"]					            =	$_POST["GENDER"];
        $admission["RELIGION"]					            =	$_POST["RELIGION"];
        $admission["CASTE"]					                =	$_POST["CASTE"];
        $admission["DISABILITY"]					        =	$_POST["DISABILITY"];
        $admission["BLOOD_GROUP"]					        =	$_POST["BLOOD_GROUP"];
        $admission["BLOOD_DONATED"]					        =	$_POST["BLOOD_DONATED"];
        $admission["UNIVERSITY"]					        =	$_POST["UNIVERSITY"];
        $admission["COLLEGE"]					            =	$_POST["COLLEGE"];
        $admission["YEAR_OF_PASSING"]					    =	$_POST["YEAR_OF_PASSING"];
        $admission["H_NM"]					            =	$_POST["H_NM"];
        $admission["H_FM"]					            =	$_POST["H_FM"];
        $admission["H_PM"]					            =	$_POST["H_PM"];
        $admission["H_MO"]					            =	$_POST["H_MO"];
        $admission["G1_NM"]					            =	$_POST["G1_NM"];
        $admission["G1_FM"]					            =	$_POST["G1_FM"];
        $admission["G1_PM"]					            =	$_POST["G1_PM"];
        $admission["G1_MO"]					            =	$_POST["G1_MO"];
        $admission["G2_NM"]					            =	$_POST["G2_NM"];
        $admission["G2_FM"]					            =	$_POST["G2_FM"];
        $admission["G2_PM"]					            =	$_POST["G2_PM"];
        $admission["G2_MO"]					            =	$_POST["G2_MO"];
        $admission["SEC_NM"]					        =	$_POST["SEC_NM"];
        $admission["SEC_FM"]					        =	$_POST["SEC_FM"];
        $admission["SEC_PM"]					        =	$_POST["SEC_PM"];
        $admission["SEC_MO"]					        =	$_POST["SEC_MO"];
        $AGGREGATE								        =	$_POST["H_MO"] + $_POST["G1_MO"] + $_POST["G2_MO"];


        $conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
        if($conn->connect_error) echo "115";//die("Connection failed: " . $conn->connect_error);
        else{
            mysqli_set_charset($conn,"utf8");
            
            $stmt = $conn->prepare("INSERT INTO ".PG21S. " (`NAME`,`MOBILE`,EMAIL,LOGIN_EMAIL,`FATHER_NAME`, `GUARDIAN_NAME`, `ADDRESS`,`DOB_DATE`,`DOB_MONTH`,`DOB_YEAR`,`GENDER`, `RELIGION`, `CASTE`, `DISABILITY`, `BLOOD_GROUP`, `BLOOD_DONATED`, `UNIVERSITY`, `COLLEGE`, `YEAR_OF_PASSING`, `H_NM`, `H_FM`, `H_PM`, `H_MO`, `G1_NM`, `G1_FM`, `G1_PM`, `G1_MO`, `G2_NM`, `G2_FM`, `G2_PM`, `G2_MO`, `AGGREGATE`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)");

            $stmt->bind_param("ssssssssssssssssssssssssssssssss", $admission["NAME"],$admission["MOBILE"],$admission["EMAIL"], $admission["LOGIN_EMAIL"], $admission["FATHER_NAME"],$admission["GUARDIAN_NAME"],$admission["ADDRESS"],$admission["DOB_DATE"],$admission["DOB_MONTH"],$admission["DOB_YEAR"],$admission["GENDER"],$admission["RELIGION"],$admission["CASTE"],$admission["DISABILITY"],$admission["BLOOD_GROUP"],$admission["BLOOD_DONATED"],$admission["UNIVERSITY"],$admission["COLLEGE"],$admission["YEAR_OF_PASSING"],$admission["H_NM"],$admission["H_FM"],$admission["H_PM"],$admission["H_MO"],$admission["G1_NM"],$admission["G1_FM"],$admission["G1_PM"],$admission["G1_MO"],$admission["G2_NM"],$admission["G2_FM"],$admission["G2_PM"],$admission["G2_MO"], $AGGREGATE);
            //echo "116";
            if($stmt->execute()){
                $echoinfo01= '<div class="container" style="max-width:768px;"><h3>Application Form Successfully Submitted !</h3><h3>Now proceed to print</p></div><div class="container" style="max-width:768px;"><a href="/Print?id='.mysqli_insert_id($conn). '" target="_blank" class="button"><span>Print Form</span></a>

                <!--<a href="/PG_Admission_19/payment_pg?id=' . mysqli_insert_id($conn) . '" target="_blank" class="button"><span>Online Payment</span></a>-->
                
                <!--<a href="/Print_chalan_pg?id='.mysqli_insert_id($conn).'" target="_blank" class="button"><span>Print Chalan</span></a>-->
                
                </div>';
            }
            else{
                echo "Error: <br> Contact Web admin " .mysqli_error($conn); exit();
            }$tsid=mysqli_insert_id($conn);
            $stmt->close();
        }$conn->close();

        $okk=0;
        
        $target_dir = getcwd(). "/uploads/photo/";
        $target_file = $target_dir . $tsid.'.jpg';
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["Photo"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "Photo -File is not an image. <br>";
                $uploadOk = 0;
            }
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Photo -File , file already exists, contact web admin <br> ";
            $uploadOk = 0;
        }
        // Check file size
        if ( $_FILES["Photo"]["size"] >150000) {
            echo "Photo -File maintain file size. <br>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" ) {
            echo "Photo -File , only JPG files are allowed. <br>";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Photo -File , your file was not uploaded. <br>";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["Photo"]["tmp_name"], $target_file)) {
                echo "<div class='container' style='max-width:768px;'><h3>The Photo -File  ". basename( $_FILES["Photo"]["name"])." has been uploaded.</h3><br></div>"; $okk=$okk+1;
            } else {
                echo "<div class='container' style='max-width:768px;'><h3>Photo -File , there was an error uploading your file.</h3><br></div>";
            }
        }																																																				
        $target_dir = getcwd(). "/uploads/sign/";
        $target_file = $target_dir . $tsid.'.jpg';
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["sign"]["tmp_name"]);
            if($check !== false) {
                echo "<h3>File is an image - " . $check["mime"] . ".</h3><br>";
                $uploadOk = 1;
            } else {
                echo "<h3>Sign - File is not an image.</h3><br>";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "<h3>Sign - , file already exists, contact web admin</h3><br>";
            $uploadOk = 0;
        }
        // Check file size
        if ( $_FILES["sign"]["size"] >150000) {
            echo "<h3>Sign - maintain file size.</h3><br>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" ) {
            echo "<h3>Sign - , only JPG files are allowed.</h3><br>";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<h3>Sign - , your file was not uploaded.</h3><br>";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["sign"]["tmp_name"], $target_file)) {
                echo "<div class='container' style='max-width:768px;'><h3>Sign - file ". basename( $_FILES["sign"]["name"]). " has been uploaded.</h3><br></div>"; $okk=$okk+1;
            } else {
                echo "<div class='container' style='max-width:768px;'><h3>Sign - , there was an error uploading your file.</h3><br></div>";
            }
        }

        $target_dir = getcwd(). "/uploads/mark/";
        $target_file = $target_dir . $tsid.'.jpg';
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["mark"]["tmp_name"]);
            if($check !== false) {
                echo "<h3>File is an image - " . $check["mime"] . ".</h3><br>";
                $uploadOk = 1;
            } else {
                echo "<h3>Marksheet -File is not an image.</h3><br>";
                $uploadOk = 0;
            }
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            echo "<h3>Marksheet file already exists, contact web admin</h3><br>";
            $uploadOk = 0;
        }
        // Check file size
        if ( $_FILES["mark"]["size"] >150000) {
            echo "<h3>Marksheet -File maintain file size.</h3><br>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" ) {
            echo "<h3>Marksheet -File , only JPG files are allowed.</h3><br>";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<h3>Marksheet -File, your file was not uploaded.</h3><br>";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["mark"]["tmp_name"], $target_file)) {
                echo "<div class='container' style='max-width:768px;'><h3>Marksheet file ". basename( $_FILES["mark"]["name"]). " has been uploaded.</h3><br></div>"; $okk=$okk+1;
            } else {
                echo "<div class='container' style='max-width:768px;'><h3>Marksheet -File, optional.</h3><br></div>";
            }
        }

        if($okk>1){
            echo $echoinfo01;
        } else echo '<h2> Fill up the admission form again </h2>';
}
?>