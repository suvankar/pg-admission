<!DOCTYPE html>
<html lang="en">
<head>

  <title>Admin Panel Amdanga College</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/asset/css/bootstrap.min.css">  
  <script src="/asset/js/jquery.min.js"></script>
  <script src="/asset/js/bootstrap.min.js"></script>
  <style>
  table th{
  vertical-align:middle !important;
  text-align:center;
  padding:6px !important;
  }
  table td{
  vertical-align:middle !important;
  text-align:center;
  padding:4px !important;
  }
  .mr{
  margin-right:20px;
  }
  </style>
</head>
<body>
<!-- nav start -->

<?php $ua = getBrowser();
if ($ua['Device'] == "Mobile") include "menu.php";
else include "menu.php";
?>
