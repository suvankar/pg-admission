<?php
	$conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
	$s= urldecode($_GET['s']);
	$sql = "SELECT * FROM `admission` WHERE `MERIT_LIST` IS NOT NULL AND `GEN_HONOURS` LIKE '%".$s."%'" ."ORDER BY HS_SUB_AGG DESC";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		echo'<div class="container table-responsive">
					<table class="table table-bordered table-striped table-hover">
						<tr>
							<th>SL</th>
							<th>FORM ID</th>
							<th>STUDENT NAME</th>
							<th>STUDENT PHOTO</th>
							<th>MOBILE NO.</th>
							<th>HONOURS</th>
							<th>AGGREGATE MARKS</th>
							<th>FORM CHALAN</th>
							<th>ADMISSION CHALAN</th>
							<th>MERIT LIST</th>
						</tr>';
			$i=0;
    while($row = mysqli_fetch_assoc($result)) {
			$i++;
			$id											=	$row["ID"];
			$STUDENT_NAME								=	$row["STUDENT_NAME"];
			$MOBILE										=	$row["MOBILE"];
			$Aggregate									=	$row["HS_SUB_AGG"];
			$GEN_HONOURS								=	$row["GEN_HONOURS"];
			$FORM_CHALAN								=	$row["FORM_CHALAN"];
			$ADMISSION_CHALAN							=	$row["ADMISSION_CHALAN"];
			$MERIT_LIST									=	$row["MERIT_LIST"];
			echo '
			<tr>
				<td>'.$i.'</td>
				<td>SCC19UG'.$id.'</td>
				<td>'.$STUDENT_NAME.'</td>
				<td style="text-align:center;"><img src="/CONTENT/ROOT_URI/Admission/uploads/photo/'.$id.'.jpg" style="width:80px;border-radius:3px;border:1px solid #f3f3f3;"></td>
				<td>'.$MOBILE.'</td>
				<td>'.$GEN_HONOURS.'</td>
				<td>'.$Aggregate.'</td>
				<td>'.$FORM_CHALAN.'</td>
				<td>'.$ADMISSION_CHALAN.'</td>
				<td>'.$MERIT_LIST.'</td>
			</tr>';


		}echo '	</table></div>';
	} else {   echo "<div class='container text-center'><h2>No students for this Stream!</h2></div>";}
?>
