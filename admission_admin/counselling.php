<link rel="stylesheet" href="/asset/css/admission_form.css" type="text/css">

<?php
if (isset($_POST["FORM_NAME_C"]) && $_POST["FORM_NAME_C"] == "FORM_NAME_C") {
	$conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
	$sql = "UPDATE `scc_pg_19` SET `COUNSELLING` = " . $_POST["COUNSELLING"] . " WHERE `ID`='" . $_POST["ROW_ID"] . "'";
	if (mysqli_query($conn, $sql)) {
		echo "<div class='container mt-3'><div class='alert alert-success alert-dismissable'>
		<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		<strong>Approved!!</strong> For Student Id No. <strong>SCC19PG" . $_POST["ROW_ID"] . "</strong></div></div>";
	} else echo "<div class='container mt-3'><div class='alert alert-danger alert-dismissable'>
		<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		<strong>Error!! </strong></div></div>";
}
?>

<?php
if (isset($_POST["FORM_NAME"]) && $_POST["FORM_NAME"]=="FORM_NAME") {
	$ID = $_POST["FORM_ID"];
	$ID = substr($ID, 7);
	$conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
	$sql = "SELECT * FROM `scc_pg_19` WHERE `ID` = '" . $ID . "'" . "ORDER BY AGGREGATE DESC";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		echo '<div class="admission_form">
				<div class="header-block">
					<div class="img-block">
						<img class="logo_img" src="/asset/img/scc_logo.png" alt="logo">
					</div>
					<div class="text-block">
						<h1>Sreechaitanya College</h1>
						<p>Habra(Arts & Science)</p>
						<p>Prafullanagar, Habra, 24PGS(N) PIN-743268, WB</p>
					</div>
				</div>
				<hr class="hr_margin"><center style="background-color:#ff9742;color:white;padding:5px;">PG ADMISSION FORM 2019-20 </center><hr class="hr_margin">';
		while ($row = mysqli_fetch_assoc($result)) {
			$STUDENT_NAME						=	$row["NAME"];
			$MOBILE								=	$row["MOBILE"];
			$EMAIL								=	$row["EMAIL"];
			$FATHER_NAME						=	$row["FATHER_NAME"];
			$GUARDIAN_NAME						=	$row["GUARDIAN_NAME"];
			$ADDRESS							=	nl2br($row["ADDRESS"]);
			$DOB								=	$row["DOB_DATE"]."/". $row["DOB_MONTH"]."/".$row["DOB_YEAR"];
			$GENDER								=	$row["GENDER"];
			$RELIGION							=	$row["RELIGION"];
			$RESERVATION_CATEGORY				=	$row["CASTE"];
			$PHYSICAL_DISABLITY					=	$row["DISABLITY"];
			$BLOOD_GROUP						=	$row["BLOOD_GROUP"];
			$UNIVERSITY						    =	$row["UNIVERSITY"];
			$COLLEGE							=	$row["COLLEGE"];
			$HONOURS						    =	$row["H_NM"];
			$HS_PASS_YEAR						=	$row["YEAR_OF_PASSING"];
			$H_NM						        =	$row["H_NM"];
			$H_MO						        =	$row["H_MO"];
			$G1_NM						        =	$row["G1_NM"];
			$G1_MO						        =	$row["G1_MO"];
			$G2_NM						        =	$row["G2_NM"];
			$G2_MO						        =	$row["G2_MO"];
			$DATE								=	substr($row["DATESTAMP"],0,10);
			$MERIT_LIST							=	$row["MERIT_LIST"];
			$Aggregate							=	$row["AGGREGATE"];

			echo '
			<table><td style="width:75%"> Session : 2019-20 </td><td style="width:25%"> Form No: SCC19PG'.$ID. '<td></table>
			
			<table width="100%">
				<tr>
					<td width="130px">
						<img src="/CONTENT/ROOT_URI/PG_Admission_19/uploads/photo/'.$ID.'.jpg" style="width:100%;height:auto;border-radius:3px;border:1px solid #f3f3f3;">
					</td>
					<td>
						<table class="table" height="150px">
							<tr>
								<td  width=50%>Student' . "'" . 's Name</td>
								<td>'.$STUDENT_NAME.'</td>							
							</tr>							
							<tr>
								<td>Applying For</td>
								<td>'.$HONOURS.'</td>
							</tr>
							<tr>
								<td>Aggregate Marks</td>
								<td>'.$Aggregate. '</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table class="table" style="text-align:left;">
			<tr>
				<td width=50%>Gender : '.$GENDER.'</td>
				<td>Father' . "'" . 's Name :- '.$FATHER_NAME.'</td>
			</tr>
			<tr>
				<td>Date Of Birth : '.$DOB.'</td>
				<td>Mobile No : '.$MOBILE.'</td>
			</tr>			
			<tr>
				<td>E-mail : '.$EMAIL.'</td>
				<td>Blood Group : '.$BLOOD_GROUP.'</td>
			</tr><tr>
				<td>Reservation Category : '.$RESERVATION_CATEGORY.'</td>
				<td>Physical Disability : '.$PHYSICAL_DISABLITY.'</td>
			</tr>
			<tr>
				<td>NAME of The Institute : '.$UNIVERSITY.'</td>
				<td>NAME of The Board : '. $COLLEGE .'</td>
			</tr>
			<tr>
				<td>Merit List : '.$MERIT_LIST.'</td>
				<td>Year of Passing : '.$HS_PASS_YEAR.'</td>
			</tr>
			<tr>				
				<td colspan="2" style="text-align:left;">Address : '.$ADDRESS.'</td>
			</tr>
		</table><br>

		<table class="table" style="width:500px; margin-left:auto;margin-right:auto;">
			<tr>
				<th>Applied Subjects</th>
				<th>Marks Obtained</th>				
			</tr>
			<tr>				
				<td width=50%>'.$H_NM.'</td>
				<td>'.$H_MO.'</td>
			</tr>
			<tr>				
				<td>'.$G1_NM.'</td>
				<td>'.$G1_MO.'</td>
			</tr>
			<tr>				
				<td>'.$G2_NM.'</td>
				<td>'.$G2_MO.'</td>
			</tr>
			<tr>
				<td>Aggregate Marks</td>
				<td>'.$Aggregate. '</td>
			</tr>
		</table><br>
		<table>
			<tr>
				<td width="500px">Date Of Submission : <?php echo $DATE;?></td>
				<td><img src="/CONTENT/ROOT_URI/PG_Admission_19/uploads/sign/'.$ID.'.jpg" style="width:auto;height:75px;"></td>
			</tr>
		 	<tr>
			 	<td>Print Date : '.date("Y-m-d").'</td><td><center>Student' . "'" . 's Signature<center></td>
			</tr>
		</table><br>
		<form method="post" enctype="multipart/form-data" class="form-horizontal">
			<input type="hidden" name="FORM_NAME_C" value="FORM_NAME_C">        
			<input type="hidden" name="ROW_ID" value="' . $ID . '">        
			<input type="hidden" name="COUNSELLING" value="'.time().'">
			<button style="padding:15px 30px;" type="submit" class="btn btn-success">Approve</button>
    	</form>
	</div><br>
			
			
			';
			}
		} else {
		echo "<div class='container text-center'><h2>Student Does not Exist!!!</h2></div>";
	}
	}
?>

<div class="container">
    <h2>SEARCH STUDENT</h2><hr>
    <form method="post" enctype="multipart/form-data" class="form-inline">
        <div class="form-group">
            <label class="control-label col-sm-4">Form Id</label>
            <div class="col-sm-8">
                <input type="hidden" name="FORM_NAME" value="FORM_NAME">
                <input type="text" class="form-control" placeholder="Enter Form Id" name="FORM_ID">
             </div>
        </div>

        <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div><br>