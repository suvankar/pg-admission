	<!--CSS for Form-->
		<link rel="stylesheet" href="/asset/css/form.css">
	<!--CSS for Form-->
	
	<header>
		<div class="wrapper">
			<h2>ADMISSION FORM 2019-20</h2>
		</div>
	</header>

    <form id="ug_admission" method="post" action="POsts" enctype="multipart/form-data" accept-charset="UTF-8">
		<input type="hidden" id="refreshed" value="no">
		<input type="hidden" name="formid" value="BMMV_UG_17">

		<!-- Personal information... -->
		<div class="wrapper bg">
			<div class="row">
				<h2 class="title">Personal Information</h2>				
				<span>
					<input class="slide-up" id="name" type="text" required name="NAME" placeholder="Your Name" />
					<label for="name">Name</label>
				</span>
				<span>
					<input class="slide-up" id="father" type="text" required name="FATHER" placeholder="Your Father's Name" />
					<label for="father">FATHER'S NAME</label>
				</span>				
				<span>
					<input class="slide-up" id="mother" type="text" name="MOTHER" placeholder="Your Mother's Name" />
					<label for="mother">MOTHER'S NAME</label>
				</span>					
				<span>
					<input class="slide-up" id="guardian" type="text" name="GUARDIAN" placeholder="Your Guardian's Name" />
					<label for="guardian">GUARDIAN'S NAME</label>
				</span>
				<div class="main">
					<div class="div-block">
						<span>
							<p class="label">GENDER</p>
							<select id="gender" class="soflow" onchange="toggle_husband_visibility(id);" name="">
								<option value="">SELECT</option>
								<option value="FEMALE">FEMALE</option>
								<option value="MALE">MALE</option>
								<option value="OTHERS">OTHERS</option>
							</select>
						</span>
					</div>
					<div class="div-block">
						<!-- Date Picker -->
						<span>
							<p class="label">DATE OF BIRTH</p>
							<table>
								<tr>
									<td>										
										<select name="DOBD" style="width:100px;" class="soflow">
											<option value="">DATE</option>
											<?php
												foreach (glob( __DIR__."/../form_settings/DOB_date/*") as $filename)
                                                {                                                   
                                                    $filename =end( explode('/', $filename));
                                                    echo '<option value="'.$filename.'">'.$filename.'</option>';
                                                }
                                            ?>
										</select>
									</td>
									<td>										
										<select name="DOBM" style="width:100px;" class="inpt_01 soflow">
											<option value="">MONTH</option>
											<?php
                                                foreach (glob( __DIR__."/../form_settings/DOB_month/*") as $filename)
                                                {                                                   
                                                    $filename =end( explode('/', $filename));
                                                    echo '<option value="'.$filename.'">'.$filename.'</option>';
                                                }
                                            ?>
										</select>										
									</td>
									<td>										
										<select name="DOBY" style="width:100px;" class="inpt_01 soflow">
											<option value="">YEAR</option>
                                            <?php
                                                foreach (glob( __DIR__."/../form_settings/DOB_year/*") as $filename)
                                                {                                                   
                                                    $filename =end( explode('/', $filename));
                                                    echo '<option value="'.$filename.'">'.$filename.'</option>';
                                                }
                                            ?>											
										</select>
									</td>
									<td>
										<span class="add_content">
										<a onclick='modalOpen("DOB_year");'>+</a>
										</span>
									</td>
								</tr>
							</table>							
						</span>
						<!-- /Date Picker -->						
					</div>
				</div>				
				<script>
					function toggle_husband_visibility(id) {
						var e = document.getElementById('gender');
						if (e.value == "FEMALE") {
							var hus_div = '<input class="slide-up" type="text" name="HUSBAND" placeholder="Your Husband\'s Name" /><label for="husband">HUSBAND\'S NAME</label>';
						}
						else {
							var hus_div = '';
						}
						document.getElementById("husband").innerHTML = hus_div;
					}						
				</script>											
				<div class="main">
					<div class="div-block">
						<span>
							<p class="label">RELIGION</p>
							<select class="soflow" name="RELIGION">
								<option value="HINDU">HINDU</option>
								<option value="MUSLIM">MUSLIM</option>
								<option value="CHRISTIAN">CHRISTIAN</option>
								<option value="JAIN">JAIN</option>
								<option value="SIKH">SIKH</option>
								<option value="BUDDHIST">BUDDHIST</option>
								<option value="OTHERS">OTHERS</option>
							</select>							
						</span>
					</div>												
					<div class="div-block">
						<span>
							<p class="label">RESERVATION CATEGORY</p>
							<select class="soflow" name="RESERVATION_CATEGORY" onchange="castc(this.value);" id="cst">
								<option value="GENERAL">GENERAL</option>
								<option value="SC">SC</option>
								<option value="ST">ST</option>
								<option value="OBC-A">OBC-A</option>
								<option value="OBC-B">OBC-B</option>
							</select>							
						</span>
					</div>
				</div>
				<div class="main">
					<div class="div-block">
						<span>
							<p class="label">PHYSICAL DISABLITY</p>
							<select class="soflow" name="PHYSICAL_DISABLITY">
								<option value="NO">No</option>
								<option value="PH_10">PH upto 10% Disablity</option>
								<option value="PH_20">PH upto 20% Disablity</option>
								<option value="PH_30">PH upto 30% Disablity</option>
								<option value="PH_40">PH upto 40% Disablity</option>
								<option value="PH_50">PH upto 50% Disablity</option>
								<option value="PH_60">PH upto 60% Disablity</option>
							</select>
						</span>
					</div>
					<div class="div-block">
						<span>
							<p class="label">BLOOD GROUP</p>
							<select class="soflow" name="BLOOD_GROUP">
								<option value="N/A">N/A</option>
								<option value="A+">A+</option>
								<option value="A-">A-</option>
								<option value="B+">B+</option>
								<option value="B-">B-</option>
								<option value="AB+">AB+</option>
								<option value="AB-">AB-</option>
								<option value="O+">O+</option>
								<option value="O-">O-</option>
							</select>
						</span>
					</div>
				</div>
				<!-- Empty Husband Div -->
				<span id="husband"></span>
				<!-- /Empty Husband Div -->
				<span>
					<input class="slide-up" id="nationality" type="text" name="NATIONALITY" placeholder="Your Nationality" value="INDIAN" />
					<label for="nationality">NATIONALITY</label>
				</span>
				<span>
					<input class="slide-up" id="address" type="text" name="ADDRESS" placeholder="Your Permanent Address" required />
					<label for="address">PERMANENT ADDRESS</label>
				</span>
				<span>
					<input class="slide-up" id="mobile" type="text" name="MOBILE" placeholder="Your Mobile Number" min="6000000000" max="9999999999" pattern="[0-9]{10}" required />
					<label for="mobile">MOBILE</label>
				</span>
				<span>
					<input class="slide-up" id="email" type="email" name="EMAIL" placeholder="Your Email Id" style="text-transform:lowercase" required />
					<label for="email">E-MAIL</label>
				</span>
				<span>
					<input class="slide-up" id="g_occupation" type="text" name="GUARDIAN_OCCUPATION" placeholder="Your Guardian's Occupation" required />
					<label for="g_occupation">GUARDIAN'S OCCUPATION</label>
				</span>
				<span>
					<input class="slide-up" id="m_income" type="text" name="MONTHLY_INCOME" placeholder="Monthly Income in Rupees" required />
					<label for="m_income">MONTHLY INCOME</label>
				</span>
			</div>
		</div>
		<!-- End of Personal information... -->

		<!-- Educational information... -->
		<div class="wrapper bg-alt">
			<div class="row">
				<h2 class="title">Educational Information</h2>
				<input type="hidden" id="tt1" name="ssub_1">
				<input type="hidden" id="tt2" name="ssub_2">
				<input type="hidden" id="tt3" name="ssub_3">

				<span>
					<input class="slide-up" id="hs_institute" type="text" name="HS_INSTITUTION" placeholder="NAME OF THE INSTITUTION FOR 10+2" required />
					<label for="hs_institute">INSTITUTE's NAME</label>					
				</span>
				<div class="main">
					<div class="div-block">
						<table>
							<tr>
								<td>						
									<p class="label">NAME OF THE BOARD / COUNCIL</p>
									<select name="HSBOARD" class="soflow">
										<?php
										foreach (glob( __DIR__."/../form_settings/BOARD/*") as $filename)
										{                                                   
											$filename =end( explode('/', $filename));
											echo '<option value="'.$filename.'">'.$filename.'</option>';
										}
										?>
									</select>
								</td>
								<td>
									<span class="add_content">
										<a onclick='modalOpen("BOARD")'>+</a>
									</span>
								</td>
							</tr>
						</table>
					</div>
					<div class="div-block">
						<table>
							<tr>
								<td>
									<p class="label">PASSING YEAR</p>
									<select name="HS_YEAR" class="soflow">
										<?php
											foreach (glob( __DIR__."/../form_settings/PASS_year/*") as $filename)
											{                                                   
												$filename =end( explode('/', $filename));
												echo '<option value="'.$filename.'">'.$filename.'</option>';
											}
										?>	
									</select>
								</td>
								<td>
									<span class="add_content">
										<a onclick='modalOpen("PASS_year")'>+</a>
									</span>
								</td>
							</tr>
						</table>
					</div>
				</div>				
				<span>
					<input class="slide-up" id="hs_exam" type="text" name="HS_EXAMINATION" placeholder="NAME OF THE EXAMINATION e.g HIGHER SECONDARY"
					 required />
					<label for="hs_exam">EXAMINATION NAME</label>
				</span>
				<span>												
					<input class="slide-up" id="roll" type="text" name="HS_ROLL" class="inpt_01" placeholder="ENTER ROLL No.">
					<label for="roll">ROLL</label>						
				</span>
				<span>												
					<input class="slide-up" id="no" type="text" name="HS_NO" placeholder="ENTER No.">
					<label for="roll">NO</label>						
				</span>				
				<span>
					<input class="slide-up" id="reg_no" type="text" name="HS_REGISTRATION_NUMBER" placeholder="ENTER Registration No." required>
					<label for="reg_no">HS REGISTRATION NUMBER</label>
				</span>
				<span>
					<input class="slide-up" id="mark_obt" type="text" name="HS_TOTAL_MARKS" placeholder="TOTAL MARKS OBTAINED" required>
					<label for="mark_obt">MARKS OBTAINED</label>
				</span>
				<span>
					<input class="slide-up" id="div_grade" type="text" name="HS_DIVISION" placeholder="PASSING DIVISION or GRADE" required>
					<label for="div_grade">DIVISION / GRADE</label>
				</span>
			</div>
		</div>
		<!-- End of Educational information... -->

		<!-- Applying Subject information... -->
		<div class="wrapper bg">
			<div class="row">
				<h2 class="title">Applying Subjects and Information</h2>
				<p>N.B. Education / Sociology : Psychology, Philosophy, Mathematics, Sociology, Education, Economics, Political Science, History
				be treated as related subject.</p>
				<div class="main">
					<div class="div-block">
						<table>
							<tr>
								<td>
									<span>
										<p class="label">APPLYING FOR</p>
										<select id="honours" name="GENORHONS" class="soflow" onchange="dizFetchsubjects(this.value);">
											<option value="NA">SELECT HONOURS</option>
											<?php
												foreach (glob( __DIR__."/../form_settings/HONOURS/*") as $filename)
												{                                                   
													$filename =end( explode('/', $filename));
													echo '<option value="'.$filename.'">'.$filename.'</option>';
												}
											?>
										</select>																				
									</span>
								</td>
								<td>
									<span class="add_content">
										<a onclick='modalOpen("HONOURS")'>+</a>
									</span>
								</td>
							</tr>
						</table>
					</div>

					<!-- Applying Subject Combination... -->							
					<div class="div-block">
						<table>
							<tr>
								<td>
									<span>
										<p class="label">SUBJECT COMBINATION</p>
										<select id="honours_subject" name="SUB_COMBINATION" class="soflow" name="">
											<option value="">SELECT SUBJECTS</option>											
										</select>
									</span>
								</td>
								<td>
									<span class="add_content">
										<a id="custom_sub_modal">+</a>
									</span>
								</td>
							</tr>
						</table>
					</div>
					<script>
						function createOption(v){
							var inDiv = document.querySelector('#honours_subject');							
							var opt = document.createElement('option');
							opt.innerHTML = v;
							opt.value = v;							
							inDiv.appendChild(opt);
						}

						function custom_sub_modal(v){
								var inDiv = document.querySelector('#custom_sub_modal');							
								inDiv.setAttribute("onclick", "modalOpen('SUB_COMBO/"+v+"')");
								inDiv.innerHTML = '+';
						}
						
						function dizFetchsubjects(v){
							if(v=="NA"){var inDiv = document.querySelector('#honours_subject');inDiv.innerHTML='<option value="">SELECT SUBJECTS</option>';}
							else{
								custom_sub_modal(v);
								var f= "/api/globe_fnames?loc=Admission/form_settings/SUB_COMBO/";

								fetch(f+v)
								.then(function(response) { return response.json(); })
								.then(function(json) {
									var inDiv = document.querySelector('#honours_subject');
									inDiv.innerHTML='<option value="">SELECT SUBJECTS</option>';
									if(json.status=='y'){									
										for(var i = 0; i < json.fname.length; i++) {
											createOption(json.fname[i]);										
										}
									}
								});
						  	}
						}												
					</script>
					<!-- End of Applying Subject Combination... -->

				</div>
			</div>
		</div>
		<!-- End of Applying Subject information... -->
				
		<!-- File Upload... -->
		<div class="wrapper bg-alt">
			<div class="row">
				<h2 class="title">File Upload</h2>
				<p>SCANNED DOCUMENTS SHOULD BE IN GREYSCALE/Black&White (149KB MAX LIMIT FOR EACH FILE).</p>
				<span>
					<input class="slide-up" type="file" name="Photo" >
					<label class="file-label">IMAGE</label>
					<label>SELECT YOUR IMAGE TO UPLOAD</label>
				</span>
				<span>
					<input class="slide-up" type="file" name="sign">
					<label class="file-label">SIGNATURE</label>
					<label>SELECT YOUR SIGNATURE TO UPLOAD</label>
				</span>
				<span>
					<input class="slide-up" type="file" name="admit">
					<label class="file-label">ADMIT CARD</label>
					<label>SELECT YOUR MADHYAMIK(10) ADMIT TO UPLOAD</label>
				</span>
				<span>
					<input class="slide-up" type="file" name="mark">
					<label class="file-label">MARK SHEET</label>
					<label>SELECT YOUR HS(10+2) MARKSHEET TO UPLOAD</label>
				</span>
				<span>
					<input class="slide-up" type="file" id="ccerti" name="cast">
					<label class="file-label">CASTE CERTIFICATE</label>
					<label>SELECT YOUR CASTE CERTIFICATE (Mandatory For SC/ST/OBC candidates)</label>
				</span>
				<div style="padding:10px; border-radius:10px; border:2px dotted green;margin-top:10px;">
					N.B.
					<br> Following documents must be submitted at the time of admission :
					<br> a) Attested true copy of Madhyamik Marksheet & Admit Card.
					<br> b) Attested true copy of H.S. Marksheet & Admit Card.
					<br> c) Attested true copy of SC / ST Certificate (Govt.)
					<br> or
					<br> original certificate of SC / ST / (M.P. / M.L.A. Certificate)
					<br> d) Two Copies of Passport Size Photograph.
					<br> e) Original School Leaving Certificate
					<br>
				</div>
				<input class="butt" type="submit">
			</div>
		</div>
	</form>

	<!-- Modal Styles -->
	<style>
	.text-center{
	text-align: center;
	}
	.custom-modal{
	z-index:3;
	display:block;
	padding-top:100px;
	position:fixed;
	left:0;
	top:0;
	width:100%;
	height:100%;
	overflow:auto;
	background-color:rgb(0,0,0);
	background-color:rgba(0,0,0,0.4)
	}
	.custom-modal-content{
	margin:auto;
	background-color:#fff;
	position:relative;
	padding:0;
	outline:0;
	width:600px;
	}
	.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
	}
	.custom-animate-top{
	position:relative;
	animation:animatetop 0.8s
	}
		@keyframes animatetop{
			from{
			top:-400px;
			opacity:0}
			to{
			top:0;opacity:1
			}
		}
	.custom-display-topright{
	position:absolute;
	right:0;
	top:0;
	}
	@media (max-width:600px){
		.custom-modal-content{
		margin:0 10px;
		width:auto!important;
		}
		.custom-modal{
		padding-top:30px;
		}
	}
	@media (max-width:768px){
		.custom-modal-content{
		width:500px;
		}
		.custom-modal{
		padding-top:50px;
		}
	}
	@media (min-width:993px){
		.custom-modal-content{
		width:900px;
		}		
	}
	</style>
	<!-- Modal Styles -->

	<!-- Modal Script -->
	<script>
	var modal_id=0;

function createButton(bname,endPoint,inDiv){
	var inDiv = document.querySelector('#'+inDiv);
	var but = document.createElement('button');
				but.id = bname;
				but.setAttribute("onclick", "removeButton('"+bname+"' , '"+endPoint+"')");
				but.style.cssText = "margin:5px";
				but.classList.add("btn");
				but.classList.add("btn-danger");
				but.innerHTML = bname + ' &nbsp;<span class="badge">X</span>';
				inDiv.appendChild(but);
}
	//Shows data in Modal
	function dizFetch(apiLink,endPoint,inDiv){
      	fetch(apiLink+endPoint)
      	.then(function(response) { return response.json(); })
      	.then(function(json) {
			if(json.status=='y'){
				for(var i = 0; i < json.fname.length; i++) {
					createButton(json.fname[i],endPoint,inDiv);

				}
			}
      	});
  	}
	//Shows data in Modal

	//Removes Modal
	function removeModal(id) {
		window.modal_id=0;
		var elem = document.getElementById(id);
		return elem.parentNode.removeChild(elem);
	}

	document.onkeydown = function keyremove(evt) {
		evt = evt || window.event;
		if (evt.keyCode == 27 & window.modal_id!==0) {removeModal(window.modal_id);}
	};
	//Removes Modal

	//Opens Modal
	function modalOpen(e) {
		window.modal_id=e;
		var modalDiv = document.createElement('div');
		modalDiv.classList.add("custom-modal");
		modalDiv.id = e;
		var modal = '<div class="custom-modal-content custom-animate-top"><div class="modal-header"><span onclick="removeModal(\''+e+'\')" class="btn custom-display-topright">&times;</span><h5 class="modal-title">EDIT</h5></div><div class="modal-body text-center"><div class="form-inline"><div class="form-group"><input type="text" id="newName" class="form-control"  placeholder="New Entry"></div><div class="form-group"><button type="button" onclick="addNewEntry('+"'newName','"+e+"','modalSpace'"+')" class="btn btn-success">Add</button></div></div><hr><small class="text-muted">Existed Entry</small><div id="modalSpace"></div></div><div class="modal-footer"><button type="button" class="btn btn-secondary" onclick="removeModal(\''+e+'\')">Close</button></div></div>';
		modalDiv.innerHTML = modal;
		document.body.appendChild(modalDiv);
		dizFetch("/api/globe_fnames?loc=Admission/form_settings/",e,"modalSpace");
	}
	
	//Add new entry
	function addNewEntry(fname,inFolder,inDiv){
		var fname=document.getElementById(fname).value;
		var f='/api/create_file?filename='+fname+'&inFolder='+inFolder;
		fetch(f)
		.then(function(response) { return response.json(); })
		.then(function(json) {
			if(json.status=='y')createButton(fname,inFolder,inDiv);
		});
	}

	//Removes a Button from Modal		
	function removeButton(fname,inFolder){
		var f='/api/delete_file?name='+fname+'&folder=CONTENT/ROOT_URI/Admission/form_settings/'+inFolder;
		fetch(f)
		.then(function(response) { return response.json(); })
		.then(function(json) {
			if(json.status=='y')removeModal(fname);
		});
	}
	</script>
	<!-- Modal Script -->
	<!-- Modal-->