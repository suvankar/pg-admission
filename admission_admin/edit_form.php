<div class="container">
    <h2>SEARCH STUDENT</h2><hr>
    <form method="post" enctype="multipart/form-data" class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Form Id</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" placeholder="Enter Form Id" name="FORM_ID">
             </div>
        </div>

        <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>

<?php
    if(isset($_POST["FORM_ID"])){
        $ID=$_POST["FORM_ID"];
        $ID=substr($ID,7);
        $conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);	    
	    $sql = "SELECT * FROM `admission` WHERE `ID` = '".$ID."'" ."ORDER BY HS_SUB_AGG DESC";
	    $result = mysqli_query($conn, $sql);
	    if (mysqli_num_rows($result) > 0) {
		echo'<hr><div class="container table-responsive text-center">
				<table class="table table-bordered table-striped table-hover">
					<tr>
						<th>FORM ID</th>
						<th>STUDENT NAME</th>							
						<th>FATHER'."'".'S NAME</th>							
						<th>DATE OF BIRTH</th>
						<th>MOBILE NO.</th>
						<th>RESERVATION CATEGORY</th>
						<th>HONOURS</th>
						<th>AGGREGATE MARKS</th>
						<th>MERIT LIST</th>
					</tr>';
    	while($row = mysqli_fetch_assoc($result)) {			
			$id											=	$row["ID"];
			$STUDENT_NAME								=	$row["STUDENT_NAME"];
			$FATHER_NAME								=	$row["FATHER_NAME"];
			$DOB								        =	$row["DOB"];
			$MOBILE										=	$row["MOBILE"];
			$RESERVATION_CATEGORY						=	$row["RESERVATION_CATEGORY"];
			$Aggregate									=	$row["HS_SUB_AGG"];
			$GEN_HONOURS								=	$row["GEN_HONOURS"];
            $MERIT_LIST									=	$row["MERIT_LIST"];
            if($MERIT_LIST == ""){$MERIT_LIST="NOT AVAILABLE";}
			echo '
			<tr>
				<td>SCC19UG'.$id.'</td>
				<td>'.$STUDENT_NAME.'</td>
				<td>'.$FATHER_NAME.'</td>
				<td>'.$DOB.'</td>
				<td>'.$MOBILE.'</td>
				<td>'.$RESERVATION_CATEGORY.'</td>
				<td>'.$GEN_HONOURS.'</td>
				<td>'.$Aggregate.'</td>
				<td>'.$MERIT_LIST.'</td>
			</tr>';


		}echo '	</table></div>';
	} else {   echo "<div class='container text-center'><h2>Student Does not Exist!!!</h2></div>";}
    }
?>