<?php
if (isset($_GET['s'])) {
	$conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
	$s = urldecode($_GET['s']);
	$sql = "SELECT * FROM `admission` WHERE `COUNSELLING` IS NOT NULL AND `GEN_HONOURS` LIKE '%" . $s . "%' ";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		echo '<div class="container">';			
    	while($row = mysqli_fetch_assoc($result)) {			
			$id											=	$row["ID"];
			$STUDENT_NAME								=	$row["STUDENT_NAME"];
			$MOBILE										=	$row["MOBILE"];
			$Aggregate									=	$row["HS_SUB_AGG"];
			$GEN_HONOURS								=	$row["GEN_HONOURS"];
			$FORM_CHALAN								=	$row["FORM_CHALAN"];
			$ADMISSION_CHALAN							=	$row["ADMISSION_CHALAN"];
			$MERIT_LIST									=	$row["MERIT_LIST"];			
			echo '
				<div class="col-sm-6 col-md-6 mb-10">
					<div class="icard">
						<div class="header-block">
							<div class="img-block">
								<img class="logo_img" src="/CONTENT/ROOT_URI/Admission/variables/amdanga_logo.png" alt="logo">
							</div>
							<div class="text-block">
								<h4>';include("CONTENT/ROOT_URI/Admission/variables/college_name.conf");echo'</h4>
								<p>';include("CONTENT/ROOT_URI/Admission/variables/college_tag.conf");echo'</p>
							</div>
						</div>
						<div class="card-body">
							<div class="id_photo">
								<img src="/CONTENT/ROOT_URI/Admission/uploads/photo/'.$id.'.jpg" alt="">
							</div>
							<div class="info">
								<p><strong>Name :- '.$STUDENT_NAME.'</strong></p>
								<p><strong>Stream :- '. $GEN_HONOURS .'</strong></p>
								<p><strong>Roll No. :- </strong></p>
								<p><strong>Mobile No. :- '. $MOBILE .'</strong></p>
								<p><strong>Session :- 2019-2020</strong></p>
								<table><td><strong>Principal'."'".'s Signature</strong></td><td><img id="sign" src="/CONTENT/ROOT_URI/Admission/uploads/sign/'.$id.'.jpg"></td></table>
							</div>
						</div>
					</div>
				</div>
			';


		}echo'</div>';
	} else {   echo "<div class='container text-center'><h2>No students for this Stream!</h2></div>";}
}
?>

<!--<div class="container">
	<div class="row">
		<div class="col-sm-6 col-md-6">
			<div class="icard">
				<div class="header-block">
					<div class="img-block">
						<a href="/"><img class="logo_img" src="/CONTENT/ROOT_URI/Admission/variables/amdanga_logo.png" alt="logo"></a>
					</div>
					<div class="text-block">
						<h4><?php include("CONTENT/ROOT_URI/Admission/variables/college_name.conf"); ?></h4>
						<p><?php include("CONTENT/ROOT_URI/Admission/variables/college_tag.conf"); ?></p>
					</div>
				</div>
				<div class="card-body">
					<div class="id_photo">
						<img src="/asset/img/avatar.png" alt="">
					</div>
					<div class="info">
						<p><strong>Name</strong></p>
						<p><strong>Course</strong></p>
						<p><strong>Roll No.</strong></p>
						<p><strong>Mobile No.</strong></p>
						<p><strong>Session</strong></p>
						<table><td><strong>Principal's Signature</strong></td><td><img id="sign" src="/asset/img/sign.jpg"></td></table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>-->

<style>
.mb-10{
margin-bottom:30px;
}
.icard{
width:500px;
height:330px;
border: 1px solid #adadad;
padding:5px;
border-radius:10px;
-webkit-box-shadow: 1px 1px 5px 1px rgba(0,0,0,0.5);
box-shadow: 1px 1px 5px 1px rgba(0,0,0,0.5);
}
.header-block{
padding: 10px;
display: table;
width:100%;
height:88px;
background:#ec9c63;
border-radius:10px;
border: 1px solid #adadad;
}
.img-block {
display: table-cell;
vertical-align: middle;
width: 30%;
text-align: center;
}
.text-block {
display: table-cell;
vertical-align: middle;
/*width: 60%;*/
}
.text-block h4 {
color: #fff;
margin-bottom: 3px;
font-family: 'Spectral SC', serif;
text-decoration: underline;
}
.logo_img {
width: 80px;
border-radius: 50%;
}
.card-body{
width:100%;
height:225px;
margin-top:5px;
padding: 10px;
background:#cce8ff;
border-radius:10px;
border: 1px solid #adadad;
}
.id_photo {
display: table-cell;
vertical-align: middle;
width: 30%;
text-align: center;
}
.info {
display: table-cell;
vertical-align: middle;
width: 70%;
padding-left:10px;
border-left: 1px solid #032ba2;
text-transform: capitalize;
}
.info p{
font-size:16px;
}
.id_photo img{
width:120px;
height:140px;
padding-right:10px;
}
#sign{
width:130px;
height:40px;
}

</style>