<nav class="navbar navbar-default">
  	<div class="container-fluid">
    	<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
      		<a class="navbar-brand" href="./">PG Admission Admin</a>
    	</div>

    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
        		<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">View Form
					<span class="caret"></span></a>
          			<ul class="dropdown-menu">
						<?php 
					foreach (glob(APP_DIR . "/CONTENT/ROOT_URI/PG_Admission_19/form_settings/HONOURS/*") as $filename) {
						$filename = explode('/', $filename);
						$filename = end($filename);
						echo "<li><a href='view_applicants?s=$filename'>$filename</a></li>";
					}
					?>
          			</ul>
        		</li>
        		<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#"> Create Merit List
					<span class="caret"></span></a>
          			<ul class="dropdown-menu">
						<?php 
					foreach (glob(APP_DIR . "/CONTENT/ROOT_URI/PG_Admission_19/form_settings/HONOURS/*") as $filename) {
						$filename = explode('/', $filename);
						$filename = end($filename);
						echo "<li><a href='merit_list?s=$filename'>$filename</a></li>";
					}
					?>
          			</ul>
        		</li>
				<li><a href="counselling">Counselling</a></li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">Departments<span class="caret"></span></a>
          			<ul class="dropdown-menu">
						<?php 
					foreach (glob(APP_DIR . "/CONTENT/ROOT_URI/PG_Admission_19/form_settings/HONOURS/*") as $filename) {
						$filename = explode('/', $filename);
						$filename = end($filename);
						echo "<li><a href='departments?s=$filename'>$filename</a></li>";
					}
					?>
          			</ul>
				</li>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">I Card<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<?php 
							foreach (glob(APP_DIR . "/CONTENT/ROOT_URI/PG_Admission_19/form_settings/HONOURS/*") as $filename) {
								$filename = explode('/', $filename);
								$filename = end($filename);
								echo "<li><a href='icard?s=$filename'>$filename</a></li>";
							}
						?>
					</ul>
				</li>
      		</ul>
			<!-- <ul class="nav navbar-nav navbar-right">
				<li><form method="post"><input type="hidden" name="SignOut" value=""><button style="margin:8px;color:#de0c0c" class="btn" type="submit">Logout</button></form></li>
			</ul> -->
			<?php
				$AvatarNLogout=APP_DIR."/CONTENT/COMPONENT/AvatarNLogout.php";
				if(file_exists($AvatarNLogout))include($AvatarNLogout); else echo 'kk'.$AvatarNLogout;
			?>
    	</div>
  	</div>
</nav>