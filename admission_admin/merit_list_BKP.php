<?php
if(isset($_POST["MERIT_LIST"]) && $_POST["MERIT_LIST"] =="NULL"){
	$conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);	
	$sql = "UPDATE `admission` SET `MERIT_LIST` = NULL WHERE `ID`='".$_POST["ROW_ID"]."'";
 	if (mysqli_query($conn, $sql)){
		echo "<div class='container mt-3'><div class='alert alert-success alert-dismissable'>
		<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		<strong>Merit List Cleared!!</strong> For Student Id No. <strong>SCC19UG".$_POST["ROW_ID"]."</strong></div></div>";
	} else echo"<div class='container mt-3'><div class='alert alert-danger alert-dismissable'>
		<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		<strong>Error!! </strong>0 Results!!</div></div>";
}
?>

<?php
$fl0 = 1;
if(isset($_POST["HS_SUB_AGG"]) && $_POST["HS_SUB_AGG"]!="" && $_POST["HS_SUB_AGG"]>1){
	$conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);	
	$sql = "UPDATE `admission` SET `MERIT_LIST` = '".$_POST["MERIT_LIST_COUNT"]."' WHERE `MERIT_LIST` IS NULL AND `HS_SUB_AGG` >= '".$_POST["HS_SUB_AGG"]."' AND `GEN_HONOURS`='".$_GET["s"]."'";
 	if (mysqli_query($conn, $sql)){
		echo "<div class='container mt-3'><div class='alert alert-success alert-dismissable'>
		<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		<strong>Done!! Merit List Created</strong></div></div>";
	} else echo"<div class='container mt-3'><div class='alert alert-danger alert-dismissable'>
		<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
		<strong>Error!! </strong>0 Results!!</div></div>";
}
?>

<?php
	$conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
	$s= urldecode($_GET['s']);
	$sql = "SELECT * FROM `admission` WHERE `GEN_HONOURS` LIKE '%".$s."%'" ."ORDER BY HS_SUB_AGG DESC";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		echo'<div class="container table-responsive text-center">
				<h1>'.$s.'</h1><hr>
				<table class="table table-bordered table-striped table-hover">
					<tr>
						<th>SL</th>
						<th>FORM ID</th>
						<th>STUDENT NAME</th>							
						<th>MOBILE NO.</th>
						<th>HONOURS</th>
						<th>AGGREGATE MARKS</th>
						<th>FORM CHALAN</th>
						<th>ADMISSION CHALAN</th>
						<th>MERIT LIST</th>
						<th>EDIT LIST</th>
					</tr>';
			$i=0;
			$t01=0;
			$t02[0]=0;
    	while($row = mysqli_fetch_assoc($result)) {
			$i++;
			$id											=	$row["ID"];
			$STUDENT_NAME								=	$row["STUDENT_NAME"];
			$MOBILE										=	$row["MOBILE"];
			$Aggregate									=	$row["HS_SUB_AGG"];
			$GEN_HONOURS								=	$row["GEN_HONOURS"];
			$FORM_CHALAN								=	$row["FORM_CHALAN"];
			$ADMISSION_CHALAN							=	$row["ADMISSION_CHALAN"];
			$MERIT_LIST									=	$row["MERIT_LIST"];
			if($t01<=$MERIT_LIST) $t01=$MERIT_LIST;//take the biggest value in to $t01
			if ($MERIT_LIST==1)$t02[1]=1;
			if ($MERIT_LIST==2)$t02[2]=2;
			if ($MERIT_LIST==3)$t02[3]=3;
			if ($MERIT_LIST==4)$t02[4]=4;
			if ($MERIT_LIST==5)$t02[5]=5;
			echo '
			<tr>
				<td>'.$i.'</td>
				<td>SCC19UG'.$id.'</td>
				<td>'.$STUDENT_NAME.'</td>				
				<td>'.$MOBILE.'</td>
				<td>'.$GEN_HONOURS.'</td>
				<td>'.$Aggregate.'</td>
				<td>'.$FORM_CHALAN.'</td>
				<td>'.$ADMISSION_CHALAN.'</td>
				<td>'.$MERIT_LIST.'</td>
				<td>
					<form method="post" enctype="multipart/form-data">						
						<input type="hidden" name="MERIT_LIST" value="NULL">
						<input type="hidden" name="ROW_ID" value="'.$id.'">
						<button type="submit" class="btn btn-primary">Remove From List</button>
					</form>
				</td>
			</tr>';


		}echo '	</table></div>';
	} else {   echo "<div class='container text-center'><h2>No students for this Stream!</h2></div>";}

			if($t01==6)die("<div class='container'><h1>Maximum allowed Merit list 5 is over</h1></div>");
			elseif($t01==5){$fl0=6;$temp="6th";}
			elseif($t01==4){$fl0=5;$temp="5th";}
			elseif($t01==3){$fl0=4;$temp="4th";}
			elseif($t01==2){$fl0=3;$temp="3rd";}
			elseif($t01==1){$fl0=2;$temp="2nd";}
			else {$fl0 = 1;$temp="1st";}
?>

<hr>
<div class="container">
<?php 
	if(isset($t02[1]) && $t02[1]==1)echo'<a class="btn btn-primary mr" href="/Admission/PRINT/merit_list?list=1&stream='.$_GET['s'].'" target="blank">Print '.$t02[1].'st Merit List</a>';
	if(isset($t02[2]) && $t02[2]==2)echo'<a class="btn btn-primary mr" href="/Admission/PRINT/merit_list?list=2&stream='.$_GET['s'].'" target="blank">Print '.$t02[2].'nd Merit List</a>';
	if(isset($t02[3]) && $t02[3]==3)echo'<a class="btn btn-primary mr" href="/Admission/PRINT/merit_list?list=3&stream='.$_GET['s'].'" target="blank">Print '.$t02[3].'rd Merit List</a>';
	if(isset($t02[4]) && $t02[4]==4)echo'<a class="btn btn-primary mr" href="/Admission/PRINT/merit_list?list=4&stream='.$_GET['s'].'" target="blank">Print '.$t02[4].'th Merit List</a>';
	if(isset($t02[5]) && $t02[5]==5)echo'<a class="btn btn-primary mr" href="/Admission/PRINT/merit_list?list=5&stream='.$_GET['s'].'" target="blank">Print '.$t02[5].'th Merit List</a>';
?>
</div>
<hr>

<div class="container">
	<form method="post" enctype="multipart/form-data">  
		<div class="form-group form-inline">
		<input type="text" class="form-control form-style form-inline" name="HS_SUB_AGG" placeholder="Aggregate Upto">
		<input type="hidden" name="MERIT_LIST_COUNT" value="<?php echo $fl0;?>">
		<button type="submit" class="btn btn-primary">Genarate <?php echo $temp;?> Merit List</button>
		</div>    
	</form>  
</div>
