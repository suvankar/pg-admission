<?php
    $payload['sub'] =" ID";
    $payload['name'] ="RISHI";
    $payload['locale'] ="ENGLISH";
    $payload['email'] ="rishu.ccp@gmail.com";

    $conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);
    else {
        mysqli_set_charset($conn, "utf8");

        $stmt = $conn->prepare("INSERT INTO " . $GLOBALS['table3'] . " (`ID`, `FULL_NAME`, `LANG`, `EMAIL`) VALUES (?, ?, ?, ?)");

        $stmt->bind_param("ssss", $payload['sub'], $payload['name'], $payload['locale'], $payload['email']);

        if ($stmt->execute()) {
            echo $payload['sub']."<br>";
            echo $payload['name']."<br>";
            echo $payload['locale']."<br>";
            echo $payload['email']."<br>";
        } else {
            echo "Error: Contact Web admin ";
        }
    $stmt->close();
    }
    $conn->close();
?>