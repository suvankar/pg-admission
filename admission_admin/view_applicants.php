<?php
if(isset($_GET['s'])){
	$conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
	$s = urldecode($_GET['s']);
	$f = urldecode($_GET['f']);
	if(isset($_GET['f']) && $_GET['f'] == "PAID"){
		$sql = "SELECT * FROM `scc_pg_19` WHERE `H_NM` LIKE '%" . $s . "%' AND `PAY_FORM` LIKE '%" . $f . "%'" . "";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			echo '<div class="container table-responsive text-center">
				<h1>' . $s . '</h1><hr>
				<table class="table table-bordered table-striped table-hover">
					<tr>
						<th>SL</th>
						<th>FORM ID</th>
						<th>STUDENT NAME</th>							
						<th>MOBILE NO.</th>
						<th>APPLIED FOR</th>
						<th>AGGREGATE MARKS</th>
					</tr>';
			$i = 0;
			$t01 = 0;
			$t02[0] = 0;
			while ($row = mysqli_fetch_assoc($result)) {
				$i++;
				$id = $row["ID"];
				$STUDENT_NAME = $row["NAME"];
				$MOBILE = $row["MOBILE"];
				$H_NM = $row["H_NM"];
				$AGGREGATE = $row["AGGREGATE"];
				$PAY_STATUS = $row["PAY_STATUS"];
				echo '
			<tr>
				<td>' . $i . '</td>
				<td><a href="/PG_Admission_19/PRINT/print_form_pg?id=' . $id . '" class="btn btn-primary" target="blank">SCC19PG' . $id . '</a></td>
				<td>' . $STUDENT_NAME . '</td>				
				<td>' . $MOBILE . '</td>
				<td>' . $H_NM . '</td>
				<td>' . $AGGREGATE . '</td>
			</tr>';


			}
			echo '	</table></div>';
		} else {
			echo "<div class='container text-center'><h2>No students for this Stream!</h2></div>";
		}
	} else{
		$sql = "SELECT * FROM `scc_pg_19` WHERE `H_NM` LIKE '%" . $s . "%'" . "";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			echo '<div class="container table-responsive text-center">
				<h1>' . $s . '</h1><hr>
				<table class="table table-bordered table-striped table-hover">
					<tr>
						<th>SL</th>
						<th>FORM ID</th>
						<th>STUDENT NAME</th>							
						<th>MOBILE NO.</th>
						<th>APPLIED FOR</th>
						<th>AGGREGATE MARKS</th>
					</tr>';
			$i = 0;
			$t01 = 0;
			$t02[0] = 0;
			while ($row = mysqli_fetch_assoc($result)) {
				$i++;
				$id = $row["ID"];
				$STUDENT_NAME = $row["NAME"];
				$MOBILE = $row["MOBILE"];
				$H_NM = $row["H_NM"];
				$AGGREGATE = $row["AGGREGATE"];
				$PAY_STATUS = $row["PAY_STATUS"];
				echo '
			<tr>
				<td>' . $i . '</td>
				<td><a href="/PG_Admission_19/PRINT/print_form_pg?id=' . $id . '" class="btn btn-primary" target="blank">SCC19PG' . $id . '</a></td>
				<td>' . $STUDENT_NAME . '</td>				
				<td>' . $MOBILE . '</td>
				<td>' . $H_NM . '</td>
				<td>' . $AGGREGATE . '</td>
			</tr>';


			}
			echo '	</table></div>';
		} else {
			echo "<div class='container text-center'><h2>No students for this Stream!</h2></div>";
		}
	}
}
?>