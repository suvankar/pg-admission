<?php

	if((isset($_POST['formid']))&&($_POST['formid']=='BMMV_UG_19'))
	{
		if($_POST['HS_SUB_NO1']=="" || $_POST['HS_SUB_NO2']=="" || $_POST['HS_SUB_NO3']=="" || $_POST['HS_SUB_NO4']=="" || $_POST['HS_SUB_NO5']=="" || $_POST['ssub_1']=="" || $_POST['ssub_2']=="" ){
			echo "<h2> Some informations are missing: <br> 1. Open the admission form again, 2. Refresh the form. 3. Fill it up with all mandatory information. </h2> "; exit();
        }        
        
        $admission["STUDENT_NAME"]					        =	$_POST["STUDENT_NAME"];
        $admission["FATHER_NAME"]					        =	$_POST["FATHER_NAME"];
        $admission["MOTHER_NAME"]					        =	$_POST["MOTHER_NAME"];
        $admission["GUARDIAN_NAME"]					        =	$_POST["GUARDIAN_NAME"];
        $admission["HUSBAND_NAME"]					        =	$_POST["HUSBAND_NAME"];
        $admission["GENDER"]					            =	$_POST["GENDER"];
        $admission["DOB"]					                =	$_POST["DOBD"]."/".$_POST["DOBM"]."/".$_POST["DOBY"];
        $admission["RELIGION"]					            =	$_POST["RELIGION"];
        $admission["NATIONALITY"]					        =	$_POST["NATIONALITY"];
        $admission["RESERVATION_CATEGORY"]					=	$_POST["RESERVATION_CATEGORY"];
        $admission["PHYSICAL_DISABLITY"]					=	$_POST["PHYSICAL_DISABLITY"];
        $admission["BLOOD_GROUP"]					        =	$_POST["BLOOD_GROUP"];
        $admission["ADDRESS"]					            =	$_POST["ADDRESS"];
        $admission["MOBILE"]					            =	$_POST["MOBILE"];
        $admission["EMAIL"]					                =	$_POST["EMAIL"];
        $admission["GUARDIAN_OCCUPATION"]				    =	$_POST["GUARDIAN_OCCUPATION"];
        $admission["MONTHLY_INCOME"]				        =	$_POST["MONTHLY_INCOME"];
        $admission["HS_INSTITUTION"]				        =	$_POST["HS_INSTITUTION"];
        $admission["HS_BOARD"]				                =	$_POST["HS_BOARD"];
        $admission["HS_PASS_YEAR"]				            =	$_POST["HS_PASS_YEAR"];
        $admission["HS_EXAM_NAME"]				            =	$_POST["HS_EXAM_NAME"];
        $admission["HS_ROLL"]				                =	$_POST["HS_ROLL"];
        $admission["HS_NO"]				                    =	$_POST["HS_NO"];
        $admission["HS_REGISTRATION_NUMBER"]				=	$_POST["HS_REGISTRATION_NUMBER"];
        $admission["HS_TOTAL_MARKS"]				        =	$_POST["HS_TOTAL_MARKS"];
        $admission["HS_DIVISION"]				            =	$_POST["HS_DIVISION"];        
        $admission["GEN_HONOURS"]				            =	$_POST["GEN_HONOURS"];
        $admission["HS_SUB_NO1"]				            =	$_POST["HS_SUB_NO1"];
        $admission["HS_SUB_NM1"]				            =	$_POST["HS_SUB_NM1"];
        $admission["HS_SUB_NO2"]				            =	$_POST["HS_SUB_NO2"];
        $admission["HS_SUB_NM2"]				            =	$_POST["HS_SUB_NM2"];
        $admission["HS_SUB_NO3"]				            =	$_POST["HS_SUB_NO3"];
        $admission["HS_SUB_NM3"]				            =	$_POST["HS_SUB_NM3"];
        $admission["HS_SUB_NO4"]				            =	$_POST["HS_SUB_NO4"];
        $admission["HS_SUB_NM4"]				            =	$_POST["HS_SUB_NM4"];
        $admission["HS_SUB_NO5"]				            =	$_POST["HS_SUB_NO5"];
        $admission["HS_SUB_NM5"]				            =	$_POST["HS_SUB_NM5"];
        $admission["HS_SUB_AGG"]				            =	$_POST["HS_SUB_NO1"]+$_POST["HS_SUB_NO2"]+$_POST["HS_SUB_NO3"]+$_POST["HS_SUB_NO4"]+$_POST["HS_SUB_NO5"];		
        $admission["sub_1"]				                    =	$_POST["ssub_1"];
        $admission["sub_2"]				                    =	$_POST["ssub_2"];
        $admission["sub_3"]				                    =	$_POST["ssub_3"];
        $admission["TIME"]	                                =   time();

        $conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
        if($conn->connect_error) die("Connection failed: " . $conn->connect_error);
        else{
            mysqli_set_charset($conn,"utf8");
                if ($_POST["GEN_HONOURS"] == 'stream6' || $_POST["GEN_HONOURS"] == 'stream8'){
                    $fp='g';
                }else $fp='h';

            $stmt = $conn->prepare("INSERT INTO ".$GLOBALS['table2']." (`STUDENT_NAME`,`FATHER_NAME`, `MOTHER_NAME`, `GUARDIAN_NAME`, `HUSBAND_NAME`, `GENDER`, `DOB`, `RELIGION`, `NATIONALITY`, `RESERVATION_CATEGORY`, `PHYSICAL_DISABLITY`, `BLOOD_GROUP`, `ADDRESS`, `MOBILE`, `EMAIL`, `GUARDIAN_OCCUPATION`, `MONTHLY_INCOME`, `HS_INSTITUTION`, `HS_BOARD`, `HS_PASS_YEAR`, `HS_EXAM_NAME`, `HS_ROLL`, `HS_NO`, `HS_REGISTRATION_NUMBER`, `HS_TOTAL_MARKS`, `HS_DIVISION`, `GEN_HONOURS`, `HS_SUB_NO1`, `HS_SUB_NM1`, `HS_SUB_NO2`, `HS_SUB_NM2`, `HS_SUB_NO3`, `HS_SUB_NM3`, `HS_SUB_NO4`, `HS_SUB_NM4`, `HS_SUB_NO5`, `HS_SUB_NM5`, `HS_SUB_AGG`, `sub_1`, `sub_2`, `sub_3`, `TIME`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            $stmt->bind_param("ssssssssssssssssssssssssssssssssssssssssss", $admission["STUDENT_NAME"],$admission["FATHER_NAME"],$admission["MOTHER_NAME"],$admission["GUARDIAN_NAME"],$admission["HUSBAND_NAME"],$admission["GENDER"],$admission["DOB"],$admission["RELIGION"],$admission["NATIONALITY"],$admission["RESERVATION_CATEGORY"],$admission["PHYSICAL_DISABLITY"],$admission["BLOOD_GROUP"],$admission["ADDRESS"],$admission["MOBILE"],$admission["EMAIL"],$admission["GUARDIAN_OCCUPATION"],$admission["MONTHLY_INCOME"],$admission["HS_INSTITUTION"],$admission["HS_BOARD"],$admission["HS_PASS_YEAR"],$admission["HS_EXAM_NAME"],$admission["HS_ROLL"],$admission["HS_NO"],$admission["HS_REGISTRATION_NUMBER"],$admission["HS_TOTAL_MARKS"],$admission["HS_DIVISION"],$admission["GEN_HONOURS"],$admission["HS_SUB_NO1"],$admission["HS_SUB_NM1"],$admission["HS_SUB_NO2"],$admission["HS_SUB_NM2"],$admission["HS_SUB_NO3"],$admission["HS_SUB_NM3"],$admission["HS_SUB_NO4"],$admission["HS_SUB_NM4"],$admission["HS_SUB_NO5"],$admission["HS_SUB_NM5"],$admission["HS_SUB_AGG"],$admission["sub_1"],$admission["sub_2"],$admission["sub_3"],$admission["TIME"]);

            if($stmt->execute()){
                $echoinfo01= 'Application Form Successfully Submitted !<br> Details is being mailed, check your inbox/update tab/junk <br> Now proceed to print <br> <a href="Print?Print='.$fp.'f&f='.mysqli_insert_id($conn).'" target="_blank"> <h2> Form </h2> </a><a href="Print?Print='.$fp.'c&c='.mysqli_insert_id($conn).'" target="_blank"> <h2> Chalan </h2> </a>';
            }
            else{
                echo "Error: <br> Contact Web admin " .mysqli_error($conn); exit();
            }$tsid=mysqli_insert_id($conn);
            $stmt->close();
        }$conn->close();        						

        $okk=0;
        
        $target_dir = getcwd()."/CONTENT/ROOT_URI/Admission/uploads/photo/";
        $target_file = $target_dir . $tsid.'.jpg';
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["Photo"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "Photo -File is not an image. <br>";
                $uploadOk = 0;
            }
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Photo -File , file already exists, contact web admin <br> ";
            $uploadOk = 0;
        }
        // Check file size
        if ( $_FILES["Photo"]["size"] >150000) {
            echo "Photo -File maintain file size. <br>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" ) {
            echo "Photo -File , only JPG files are allowed. <br>";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Photo -File , your file was not uploaded. <br>";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["Photo"]["tmp_name"], $target_file)) {
                echo "<h3>The Photo -File  ". basename( $_FILES["Photo"]["name"])." has been uploaded.</h3><br>"; $okk=$okk+1;
            } else {
                echo "<h3>Photo -File , there was an error uploading your file.</h3><br>";
            }
        }																																																				
        $target_dir = getcwd()."/CONTENT/ROOT_URI/Admission/uploads/sign/";
        $target_file = $target_dir . $tsid.'.jpg';
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["sign"]["tmp_name"]);
            if($check !== false) {
                echo "<h3>File is an image - " . $check["mime"] . ".</h3><br>";
                $uploadOk = 1;
            } else {
                echo "<h3>Sign - File is not an image.</h3><br>";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "<h3>Sign - , file already exists, contact web admin</h3><br>";
            $uploadOk = 0;
        }
        // Check file size
        if ( $_FILES["sign"]["size"] >150000) {
            echo "<h3>Sign - maintain file size.</h3><br>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" ) {
            echo "<h3>Sign - , only JPG files are allowed.</h3><br>";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<h3>Sign - , your file was not uploaded.</h3><br>";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["sign"]["tmp_name"], $target_file)) {
                echo "<h3>Sign - file ". basename( $_FILES["sign"]["name"]). " has been uploaded.</h3><br>"; $okk=$okk+1;
            } else {
                echo "<h3>Sign - , there was an error uploading your file.</h3><br>";
            }
        }																																																																	
        $target_dir = getcwd()."/CONTENT/ROOT_URI/Admission/uploads/admit/";
        $target_file = $target_dir .$tsid.'.jpg';
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["admit"]["tmp_name"]);
            if($check !== false) {
                echo "<h3>File is an image - " . $check["mime"] . ".</h3><br>";
                $uploadOk = 1;
            } else {
                echo "<h3>Admit -File is not an image.</h3><br>";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "<h3>Admit -, file already exists, contact web admin</h3><br> ";
            $uploadOk = 0;
        }
        // Check file size
        if ( $_FILES["admit"]["size"] >150000) {
            echo "<h3>Admit File - maintain file size.</h3><br>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" ) {
            echo "<h3>Admit -, only JPG files are allowed.</h3><br>";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<h3>Admit -, your file was not uploaded.</h3><br>";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["admit"]["tmp_name"], $target_file)) {
                echo "<h3>Admit file ". basename( $_FILES["admit"]["name"]). " has been uploaded.</h3><br>"; $okk=$okk+1;
            } else {
                echo "<h3>Admit -, there was an error uploading your file.</h3><br>";
            }
        }

        $target_dir = getcwd()."/CONTENT/ROOT_URI/Admission/uploads/mark/";
        $target_file = $target_dir . $tsid.'.jpg';
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["mark"]["tmp_name"]);
            if($check !== false) {
                echo "<h3>File is an image - " . $check["mime"] . ".</h3><br>";
                $uploadOk = 1;
            } else {
                echo "<h3>Marksheet -File is not an image.</h3><br>";
                $uploadOk = 0;
            }
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            echo "<h3>Marksheet file already exists, contact web admin</h3><br>";
            $uploadOk = 0;
        }
        // Check file size
        if ( $_FILES["mark"]["size"] >150000) {
            echo "<h3>Marksheet -File maintain file size.</h3><br>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" ) {
            echo "<h3>Marksheet -File , only JPG files are allowed.</h3><br>";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<h3>Marksheet -File, your file was not uploaded.</h3><br>";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["mark"]["tmp_name"], $target_file)) {
                echo "<h3>Marksheet file ". basename( $_FILES["mark"]["name"]). " has been uploaded.</h3><br>"; $okk=$okk+1;
            } else {
                echo "<h3>Marksheet -File, there was an error uploading your file.</h3><br>";
            }
        }

        if($_POST["RESERVATION_CATEGORY"] != "GENERAL"){
        
        $target_dir = getcwd()."/CONTENT/ROOT_URI/Admission/uploads/cast/";
        $target_file = $target_dir . $tsid.'.jpg';
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["cast"]["tmp_name"]);
            if($check !== false) {
                echo "<h3>File is an image - " . $check["mime"] . ".</h3><br>";
                $uploadOk = 1;
            } else {
                echo "<h3>Cast Certificate -File is not an image.</h3><br>";
                $uploadOk = 0;
            }
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            echo "<h3>Cast Certificate file already exists, contact web admin</h3><br>";
            $uploadOk = 0;
        }
        // Check file size
        if ( $_FILES["cast"]["size"] >150000) {
            echo "<h3>Cast Certificate -File maintain file size.</h3><br>";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" ) {
            echo "<h3>Cast Certificate -File , only JPG files are allowed.</h3><br>";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "<h3>Cast Certificate -File, your file was not uploaded.</h3><br>";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["cast"]["tmp_name"], $target_file)) {
                echo "<h3>Cast Certificate file ". basename( $_FILES["cast"]["name"]). " has been uploaded.</h3><br>";
            } else {
                echo "<h3>Cast Certificate -File, there was an error uploading your file.</h3><br>";
            }
        }
        }
													
													
        if($okk>3){
            echo $echoinfo01;
            $to = $_POST["EMAIL"];
            $subject = "Admission information 2017-19 ,Banipur Mahila Mahavidyalaya";
            $message = '
                <html>
                    <head>
                    <title>Admission information 2017-19 ,Banipur Mahila Mahavidyalaya</title>
                    </head>
                    <body>
                        <div style="margin:12px;font-size:1.3em;color:grey;">

                            <center> <a href="http://banipurmmv.in/" class="navbar-logo"><img src="http://banipurmmv.in/assets/images/c-logo.jpg" alt="Banipur Mahila Mahavidyalaya"></a> </center>

                            <br>Hi,

                            <br>Online form submission success !! <br>


                            <div style="margin:32px;color:#1591f3;">
                                <h3> Name :- '.$_POST["STUDENT_NAME"].'</h3>
                                1. Form No:- BM17UG'.$tsid.'

                            </div>
                            <br><br><br>
                        </div>
                    </body>
                </html>';

            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // More headers
            $headers .= 'From: BMMV Admission <mail@banipurmmv.in>' . "\r\n";
            $headers .= 'Reply-To: rishu.ccp@gmail.com'. "\r\n";
            //$headers .= 'Cc: myboss@example.com' . "\r\n";

             mail($to,$subject,$message,$headers);
        } 
        else echo '<h2> Fill up the admission form again </h2>';
}
?>