<?php
 if(isset($_GET['id']) && $_GET['id']>10){}else die('Form Not Found!!!');
?>
<div class="container text-center">
    <h1>Online Payment(Admission Fee)<br>for Form No . <?php echo 'SCC19PG' . $_GET['id']; ?> </h1>
    <h4>After successful payment <a href="/PG_Admission_19/payment_pg_update?chalan=form&id=<?php echo $_GET['id'] ?>" class="btn btn-primary">Click here</a> and submit the details.</h4>
    <div  class="alert alert-success"><strong><h3><strong>Mode of Payment: Google Tez / PhonePe / Paytm / NEFT / IMPS </strong></h3></strong>
    Debit / credit card can be used through Paytm<br/> * Do Not Pay to any paytm No.
    </div>
    <hr>
    <strong><h3 class="alert alert-success"><strong>*Mention the Form No.</strong> in Payment remarks while making transaction (Payment).</h3></strong><hr>
    <hr>

    <div class="well">
        <strong><h3 class="alert alert-danger">Option 1: QR (<strong>Rs <?php echo $_GET['amount']; ?>+36</strong> Updates within 2 hours)</h3></strong><hr>
        <img class="img-responsive" src="/asset/img/upi_7001601485.png" alt="upi" style="margin:auto">
    </div>

    <div class="well">
        <strong><h3 class="alert alert-danger">Option 2: UPI/Paytm (<strong>Rs <?php echo $_GET['amount']; ?>+36</strong> Updates within 2 hours)</h3></strong><hr>
        <h1 class="text-primary">UPI ID - sarkar.suvankar@okaxis</h1>
    </div>

    <div class="well">
        <strong><h3 class="alert alert-danger">Option 3: NEFT/IMPS</h3></strong><hr>
        <h1 class="text-primary"> NEFT/IMPS </h1>
		 <h4 class="text-primary"> (This method takes long time to get confirmation from the bank!) </h4>
        <h1 class="text-primary">Account No - 1595010061755</h1>
        <h4 class="text-primary">IFSC Code - UTBI0SCGF71(UBI)</h4>
        <h3 class="text-primary"><strong>Rs <?php echo $_GET['amount']; ?>+5 </strong> to be credited to the above account by NEFT.</h3>
        <strong><p class="text-danger">Note: It may take up to 3 days to reflect the update in your application</p></strong>
    </div>
</div>
