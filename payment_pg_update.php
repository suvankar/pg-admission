<div class="container">
    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label>Payment Confirmation Details (enter details how you made the payment).</label>
            <div class="form-group">
                <input type="hidden" name="formid" value="BMMV_PG_19">
                <input type="hidden" name="ID" value="<?php echo $_GET['id'] ?>"> 
                <textarea class="form-control" name="PAY_STATUS" rows="5" required>
Mode of Payment: =
Amount =
Date of Transaction =
Exact Time  of Transaction=
From No=
                </textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>

<?php

if ((isset($_POST['formid'])) && ($_POST['formid'] == 'BMMV_PG_19')) {
    $paystatus = $_POST["PAY_STATUS"];
    $id = $_POST["ID"];
    $host = $GLOBALS['host'];
    $user = $GLOBALS['user'];
    $pass = $GLOBALS['pass'];
    $db = $GLOBALS['db'];

    // $conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    // if ($conn->connect_error) die("Connection failed: " . $conn->connect_error);
    // else {
    //     mysqli_set_charset($conn, "utf8");

    //     $stmt = $conn->prepare("UPDATE `scc_pg_19` SET `PAY_STATUS` = '?' WHERE `ID` =?");
    //     $stmt->bind_param("si", $paystatus,$id);

    //     if ($stmt->execute()) {
    //         echo '<div class="alert alert-success"><h3>Updated Successfully!!</h3></div>';
    //     } else {
    //         echo "Error: <br> Contact Web admin " . mysqli_error($conn);
    //         exit();
    //     }
    //     $stmt->close();
    // }
    // $conn->close();

    try {
        $conn = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
    // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $sql = "UPDATE `scc_pg_19` SET `PAY_STATUS` = '". $paystatus ."' WHERE `scc_pg_19`.`ID` = $id";

    // Prepare statement
        $stmt = $conn->prepare($sql);

    // execute the query
        $stmt->execute();

    // echo a message to say the UPDATE succeeded
        echo '<div class="container alert alert-success"><h3>Updated Successfully!!</h3></div>';
    } catch (PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }

    $conn = null;
}    
?>